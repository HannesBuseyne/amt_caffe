"""
Created : Februari 20, 2016

Author: Hannes Buseyne
"""

import csv
import fnmatch
import os
import math

import matplotlib.pyplot as plt
import numpy as np
from scipy import signal
from scipy.io import wavfile
from scipy.signal import savgol_filter
import itertools

from data_preparation.qspectro import generate_and_plot_qgram, generate_qgram
from visualisation.visualisation import plot_weightmatrix,plot_1d_frame


def get_ISOL_NO_files(loudness="F", sustain=0):
    filter = "ISOL_NO_" + str(loudness) + '_S' + str(sustain)
    matches = []
    for root, dirnames, filenames in os.walk('/mnt/DATA/Thesis/MAPS data/'):
        for filename in fnmatch.filter(filenames, "*.wav"):
            if filter in filename:
                matches.append(os.path.join(root, filename))
    return matches


def get_note_number(filename):
    return int(filename.split('_')[5].split('M')[1])


def read_and_transform(filename):
    [fs, audio_stereo] = wavfile.read(filename)
    Xs = np.mean(audio_stereo, axis=1)
    return generate_qgram(Xs, fs)


def compare_pianos():
    songs = get_ISOL_NO_files()
    for i in range(120):
        note_songs = list(song for song in songs if get_note_number(song) == i)
        if len(note_songs) > 0:
            f, axes = plt.subplots(1, len(note_songs), sharey=True)
            for index, piano_note in enumerate(note_songs):
                t1, f1, QSxx = read_and_transform(piano_note)
                QSxx = QSxx / np.max(QSxx)  # this makes the range 0 .. 1
                fi1 = np.arange(0, len(f1))
                axes[index].pcolormesh(t1, fi1, QSxx, cmap='Blues')
                axes[index].set_title(piano_note.split("/")[-4])
            plt.show()


def __main__():
    matches = []
    threshold = 0.5
    spec_dim = 6
    S = np.zeros(shape=(112, 112))
    for root, dirnames, filenames in os.walk('/mnt/DATA/Thesis/MAPS data/'):
        for filename in fnmatch.filter(filenames, "*.wav"):
            if 'ISOL_NO' in filename:
                matches.append(os.path.join(root, filename))
    for song in matches:
        [fs, audio_stereo] = wavfile.read(song)
        Xs1 = np.mean(audio_stereo, axis=1)
        song_note = int(song.split('_')[5].split('M')[1])
        with open(song.replace(".wav", ".txt")) as tsv:
            stats1 = list(line for line in csv.reader(tsv, dialect="excel-tab"))[1:]
        t, f, QSxx1 = generate_qgram(Xs1[float(stats1[0][0]) + 0.5 * fs:float(stats1[0][0]) + 1.5 * fs], fs)
        for song2 in matches:
            if song2 != song:
                song2_note = int(song2.split('_')[5].split('M')[1])
                if S[song2_note, song_note] == 0:
                    print "note " + str(song_note) + " and note " + str(song2_note)
                    with open(song2.replace(".wav", ".txt")) as tsv:
                        stats2 = list(line for line in csv.reader(tsv, dialect="excel-tab"))[1:]
                    [fs, audio_stereo] = wavfile.read(song2)
                    Xs2 = np.mean(audio_stereo, axis=1)
                    t, f, QSxx2 = generate_qgram(Xs2[float(stats2[0][0]) + 0.5 * fs:float(stats2[0][0]) + 1.5 * fs], fs)
                    if song2_note >= song_note:
                        cut_freq = (int(song2_note) - 21) * spec_dim + (spec_dim * 2 - 1) - 3
                        QSxx2 = QSxx2 / np.max(QSxx2[cut_freq:, :])
                        QSxx1b = QSxx1 / np.max(QSxx1[cut_freq:, :])
                        QSxx3 = np.logical_and(QSxx1b > threshold, QSxx2 > threshold)
                        S[song2_note, song_note] = float(np.sum(QSxx3[cut_freq:, :])) / np.sum(
                            QSxx2[cut_freq:, :] > threshold)
                        S[song_note, song2_note] = float(np.sum(QSxx3[cut_freq:, :])) / np.sum(
                            QSxx2[cut_freq:, :] > threshold)
                    else:
                        cut_freq = (int(song_note) - 21) * spec_dim + (spec_dim * 2 - 1) - 3
                        QSxx2 = QSxx2 / np.max(QSxx2[cut_freq:, :])
                        QSxx1b = QSxx1 / np.max(QSxx1[cut_freq:, :])
                        QSxx3 = np.logical_and(QSxx1b > threshold, QSxx2 > threshold)
                        S[song2_note, song_note] = float(np.sum(QSxx3[cut_freq:, :])) / np.sum(
                            QSxx1[cut_freq:, :] > threshold)
                        S[song_note, song2_note] = float(np.sum(QSxx3[cut_freq:, :])) / np.sum(
                            QSxx1[cut_freq:, :] > threshold)
    plot_weightmatrix(S, title="similarity")


def get_tone_frequencies(transf):
    # return (np.diff(np.sign(np.diff(transf[:,0]))) < 0).nonzero()[0] + 1
    plt.plot(savgol_filter(transf[:, 0], 11, 2))
    # return argrelextrema(transf[:,0], np.greater,order=5)
    return signal.find_peaks_cwt(transf[:, 0], np.arange(3, 10))


def visualize():
    [fs, audio_stereo] = wavfile.read(
        '/mnt/DATA/Thesis/MAPS data/AkPnBcht/ISOL/NO/MAPS_ISOL_NO_F_S0_M37_AkPnBcht.wav')
    Xs = np.mean(audio_stereo, axis=1)  # audio_stereo[:,0]
    X=np.arange(0,float(Xs.shape[0])/fs,float(1)/fs)
    plt.plot(X,Xs)
    plt.ylabel('Amplitude')
    plt.xlabel('Time[sec] ')
    plt.show()
    generate_and_plot_qgram(Xs, fs, 'F maj piano spectogram')
    # [fs, audio_stereo] = wavfile.read(
    #     '/mnt/DATA/Thesis/MAPS data/AkPnBcht/ISOL/NO/MAPS_ISOL_NO_F_S0_M37_AkPnBcht.wav')
    # Xs = np.mean(audio_stereo, axis=1)  # audio_stereo[:,0]
    t, f, QSxx = generate_qgram(Xs, fs)
    # print QSxx.shape
    # QSxx = np.gradient(QSxx, axis=0)
    # plot_qgram(t, f, QSxx, 'F maj piano spectogram whitened')
    # generate_and_plot_qgram(Xs,fs,'Qgram')
    # [fs, audio_stereo] = wavfile.read('/mnt/DATA/Thesis/MAPS data/AkPnBcht/ISOL/NO/MAPS_ISOL_NO_F_S0_M69_AkPnBcht.wav')
    # Xs1 = np.mean(audio_stereo, axis=1)  # audio_stereo[:,0]
    # with open('/mnt/DATA/Thesis/MAPS data/AkPnBcht/ISOL/NO/MAPS_ISOL_NO_F_S0_M23_AkPnBcht.wav'.replace(".wav",
    #                                                                                                    ".txt")) as tsv:
    #     stats = list(line for line in csv.reader(tsv, dialect="excel-tab"))[1:]
    # print stats
    # generate_and_plot_spectrogram(Xs,fs,'Monophonic A piano spectogram')
    # [fs, audio_stereo] = wavfile.read('/mnt/DATA/Thesis/MAPS data/AkPnBcht/ISOL/NO/MAPS_ISOL_NO_F_S0_M94_AkPnBcht.wav')
    # Xs2 = np.mean(audio_stereo, axis=1)  # audio_stereo[:,0]
    # generate_and_plot_spectrogram(Xs,fs,'Monophonic A piano spectogram')
    # print get_tone_frequencies(generate_qgram(Xs1[float(stats[0][0]) + 0.5 * fs:float(stats[0][0]) + 1.5 * fs], fs)[2])
    # generate_and_plot_qgrams(Xs1[float(stats[0][0]) + 0.5 * fs:float(stats[0][0]) + 1.5 * fs],
    #                          Xs2[float(stats[0][0]) + 0.5 * fs:float(stats[0][0]) + 1.5 * fs], fs, 'Qgram', cutoff=69)
    # [fs, audio_stereo] = wavfile.read('/home/hannes/Downloads/153588__carlos-vaquero__violin-a-4-tenuto-non-vibrato.wav')
    # Xs = np.mean(audio_stereo, axis=1)  # audio_stereo[:,0]
    # # generate_and_plot_spectrogram(Xs, fs, 'Monophonic A violin spectogram')
    # generate_and_plot_qgram(Xs,fs,'Qgram')


    # [fs, audio_stereo] = wavfile.read('/mnt/DATA/Thesis/MAPS data/AkPnBcht/ISOL/NO/MAPS_ISOL_NO_F_S0_M104_AkPnBcht.wav')
    # Xs = np.mean(audio_stereo, axis=1)  # audio_stereo[:,0]
    # generate_and_plot_spectrogram(Xs,fs,'Spectogram')
    # generate_and_plot_qgram(Xs,fs,'Qgram')


def whiten(X, fudge=1E-18):
    # the matrix X should be observations-by-components

    # get the covariance matrix
    Xcov = np.dot(X.T, X)

    # eigenvalue decomposition of the covariance matrix
    d, V = np.linalg.eigh(Xcov)

    # a fudge factor can be used so that eigenvectors associated with
    # small eigenvalues do not get overamplified.
    D = np.diag(1. / np.sqrt(d + fudge))

    # whitening matrix
    W = np.dot(np.dot(V, D), V.T)

    # multiply by the whitening matrix
    X_white = np.dot(X, W)

    return X_white, W


def svd_whiten(X):
    U, s, Vt = np.linalg.svd(X)

    # U and Vt are the singular matrices, and s contains the singular values.
    # Since the rows of both U and Vt are orthonormal vectors, then U * Vt
    # will be white
    X_white = np.dot(U, Vt)

    return X_white



def overlapping_frequencies():
    fund_freqs = [16.35, 17.32, 18.35, 19.45, 20.60, 21.83, 23.12, 24.50, 25.96, 27.50, 29.14, 30.87]
    all_funds_logscale = list(pow(2,i) * ground for i in range(0, 8) for ground in fund_freqs)
    print all_funds_logscale
    precision=float(1)/72 #12 notenumbers with 6 bins
    print precision
    overlap_matrix = np.zeros(shape=(len(all_funds_logscale), len(all_funds_logscale)))
    overlap_distribution = np.zeros(shape=(len(all_funds_logscale)))
    for index_i, i in enumerate(all_funds_logscale):
        freq_lines_i = list(float(math.log(factor*i,2)) for factor in range(1,11))
        # print freq_lines_i
        for index_j, j in enumerate(all_funds_logscale):
            overlap=0
            freq_lines_j = list(float(math.log(factor*j,2)) for factor in range(1, 101))
            overlapping_lines = []
            for lines in itertools.product(freq_lines_i,freq_lines_j):
                if abs(lines[0]-lines[1]) < precision and not(lines[1] in overlapping_lines):
                    overlapping_lines.append(lines[1])
                    # print lines
                    overlap+=1
            overlap_distribution[abs(index_i-index_j)]+=overlap
            #overlap of frequency lines from note i with note j
            overlap_matrix[index_i, index_j] = float(overlap) / 10
    plot_weightmatrix(overlap_matrix)
    with open('/mnt/DATA/Thesis/frequenties_overlap.dat','w') as write_file:
        write_file.write('note_distance percentage_overlap \n')
        for i in range(63):
            #we write te overlap of frequency lines from different notes with note 44
            write_file.write(str(-31+i) + ' ' + str(overlap_matrix[13+i,44]) + '\n')

    plot_1d_frame(overlap_matrix[:,44],title="overlap distribution")


if __name__ == "__main__":
    # __main__()
    # visualize()
    # compare_pianos()
    overlapping_frequencies()
