import sys

import numpy as np

AMT_root = '/workspace/AMT_caffe/'
sys.path.extend([AMT_root + 'code/runner/',
                 AMT_root + "code/data_preparation/",
                 AMT_root + "code/python_models/",
                 AMT_root + "code/solvers/",
                 AMT_root + "code/visualisation/"])
from data_loader import MAPS_generators, MusicNet_generators
from visualisation import plot_1d_frame


def get_MAPS_stats():
    maps = MAPS_generators("MUS_1", {"MAPS_excerpt": "None", "random_minute": "True"})
    produce_stats([maps.get_train_generator(), maps.get_validation_generator(), maps.get_test_generator()], "MAPS")


def get_MusicNet_stats(part):
    musicNet = MusicNet_generators(part, {"MusicNet_excerpt": "None", "random_minute": "True"})
    produce_stats([musicNet.get_train_generator(), musicNet.get_validation_generator(), musicNet.get_test_generator()],
                  "MusicNet")


def produce_stats(generators, name):
    number_of_notes = 0
    total_length = 0
    audio_time = 0
    polyphony = 0
    nb_frames = 0
    nb_pieces = 0
    for gen in generators:
        for (Xs, Ys, filename, fs, start) in gen:
            nb_pieces +=1
            # print Xs.shape
            audio_time +=float(Xs.shape[0])/44100
            for label in Ys:
                note_length = float(label[1]) - float(label[0])
                total_length += note_length
                number_of_notes += 1
            for i in range(int(round(Xs.shape[0]/(44100)*25))):
                nb_frames +=1
                labels = list(1 for label in Ys if float(label[0])<float(i)/25 and float(label[1])>float(i)/25)
                print len(labels)
                polyphony += len(labels)

    # notes_array = np.zeros(100)
    # with open('/mnt/DATA/Thesis/figures/' + name + '_length_distribution.dat','w') as dist_file:
    #     for i in notes_per_frame_length:
    #         if int(i) < 100:
    #             notes_array[int(i)] = notes_per_frame_length[i]
    #             dist_file.write(str(i) + ' ' + str(notes_per_frame_length[i]) + '\n')
    # plot_1d_frame(notes_array, title="note_length_distribution_" + name, savefolder='/mnt/DATA/Thesis/figures', format=".png",
    #               ylabel="nb of examples",
    #               xlabel="MIDI note number")
    print "number of pieces : " + str(nb_pieces)
    print "mean length in seconds : " + str(float(total_length) / (number_of_notes))
    print "total number of notes : " + str (number_of_notes)
    print "total audiotime : " + str(audio_time)
    print "average polyphony : " + str(float(polyphony/nb_frames))

get_MAPS_stats()
get_MusicNet_stats("Solo Piano")
get_MusicNet_stats("MUS")

