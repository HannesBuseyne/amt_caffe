"""
Created : December 24, 2016

Author: Hannes Buseyne
"""



class Data:
    """
    This class represents the data that is used for training, validation and testing
    """

    def __init__(self):
        """
        makes a data object that will provide the data to train,validate and test on
        """

    def get_data(self,batch_size):
        """
        get's a batch of training data and labels in numpy format, this will shuffle the data if it went through all examples
        :param batch_size: the size of the batch (number of examples), this will shuffle the data if it went through all examples
        :return: examples,true_labels both numpy arrays with *batch size* rows the examples given depend on the training phase
        """
        raise Exception("this should be overwritten")

    def get_number_of_examples(self):
        """
        returns the number of training examples
        :return: an integer describing the number of training examples
        """
        raise Exception("this should be overwritten")

    def get_data_distribution(self):
        """
        returns a distribution of the number of examples per label
        :return: list of number of examples per label (labelnumber is index of list)
        """
        raise Exception("this should be overwritten")