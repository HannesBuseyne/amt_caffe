"""
Created : December 24, 2016

Author: Hannes Buseyne
"""
import csv
import fnmatch
import os
import random

random.seed(13)

import numpy as np
import pandas as pd
import system_settings
from scipy.io import wavfile
from sklearn.model_selection import train_test_split

from data import Data
import preprocessors
from visualisation import plot_1d_frame
from configs import parse_int_config
from configs import parse_bool_config


def prepare_data(data_source, part, data_preparation):
    """
    This function prepares the data if it is not present.
    """
    if (not (os.path.exists(system_settings.get_data_path(data_source, part, data_preparation)))):
        print("[DATA] prepared data not found, preparing data...")
        if data_source == "MAPS":
            generators = MAPS_generators(part, data_preparation.get_load_config())
        elif data_source == "MusicNet":
            # noinspection PyUnresolvedReferences
            from intervaltree import Interval, IntervalTree
            generators = MusicNet_generators(part, data_preparation.get_load_config())
        print("[DATA] saving data to numpy arrays...")
        data_preparation.save_numpy(generators.get_train_generator(), generators.get_validation_generator(),
                                    generators.get_test_generator(), data_source, part, data_preparation)
        print("[DATA] data preparation done.")


class NumpyData(Data):
    """
    An object that imports the MAPS data and is able to preprocess it
    """
    def __init__(self,phase,data_path,preprocessor):
        """
        this returns an object that will provide the MAPS data for the deep learning algorithm
        :param parts: the parts of the data, this can either be MUS for songs, ISOL for monophone music, UCHO for western chords or RAND for chords with random pitch notes.
        :param phase: the phase this data is used in : either train, validation or test
        """
        self.preprocessor = preprocessors.get_preprocessor(preprocessor)

        # matches = []
        data = str(data_path) + "/" + str(phase) + "/"
        files = [file.split("examples_")[1] for file in os.listdir(data) if file.startswith("examples_")]
        # this argument below is needed if your RAM is low => train on less data
        # if len(files) > 100:
        #     files = files[0:100]
        # if phase != "train" and len(files) > 20:
        #     files = files[0:20]
        # vstack seems to be way more memory efficient
        print("[DATA] loading and preprocessing data...")
        self.examples = np.vstack([np.load(data + "examples_" + file) for file in files])
        self.labels = np.vstack([np.load(data + "labels_" + file) for file in files])
        self.examples = self.preprocessor.apply(self.examples)
        #TODO:fix this hack for attention labels processing
        if self.preprocessor.do_labels() and "attention" in data:
            self.labels = self.preprocessor.apply(self.labels)
        self.phase = phase
        self.data_path = data_path
        if phase != "test":
            self.shuffle_in_unison(self.examples,self.labels)
        self.current = 0
        self.plot_data_distribution()
        print("[DATA] data loaded : " + str(self.examples.shape[0]) + " examples")
        print("[DATA] labels loaded : " + str(self.labels.shape[0]) + " examples")


    def get_data(self,batch_size):
        """
        get's a batch of training data and labels in numpy format, this will shuffle the data if it went through all examples
        :return: training_example,true_labels both numpy arrays
        """
        self.current += batch_size
        if self.current > self.examples.shape[0]:
            if self.phase != "test":
                self.shuffle_in_unison(self.examples, self.labels)
            self.current = batch_size
        return (np.expand_dims(np.expand_dims(self.examples[self.current-batch_size:self.current,:],axis=1),axis=1),self.labels[self.current-batch_size:self.current,:])

    def get_number_of_examples(self):
        """
        returns the number of training examples
        :return: an integer describing the number of training examples
        """
        return self.examples.shape[0]

    def get_numer_of_batches(self,batch_size):
        """
        returns number of batches to go through the dataset
        :param batch_size: the batch size
        :return: the number of batch sizes in the whole dataset
        """
        return self.examples.shape[0]/batch_size

    def get_frame_width(self):
        """
        returns the width of the example frames
        :return: an integer desribing the width of the example frames
        """
        return self.examples.shape[1]

    def get_label_width(self):
        """
        returns the width of the labels
        :return: an integer describing the width of the labels
        """
        return self.labels.shape[1]

    def get_data_distribution(self):
        """
        returns a distribution of the number of examples per label
        :return: list of number of examples per label (labelnumber is index of list)
        """
        return np.sum(self.labels,axis=0)

    def plot_data_distribution(self):
        data = self.get_data_distribution()
        plot_1d_frame(data, title="data distribution_" + self.phase, savefolder=self.data_path, format=".png",
                      ylabel="nb of examples",
                      xlabel="MIDI note number")

    def shuffle_in_unison(self,a, b):
        """
        shuffles 2 numpy arrays in unison => thus the rows are shuffled in the same order
        The arrays need to have the same number of rows
        :param a: first numpy array
        :param b: second numpy array
        """
        rng_state = np.random.get_state()
        np.random.shuffle(a)
        np.random.set_state(rng_state)
        np.random.shuffle(b)




class MAPS_generators:
    def __init__(self, part, load_config):
        global system_config
        matches = []
        print("[DATA] searching MAPS files...")
        if part.startswith("MUS_"):
            with open(system_settings.get_MAPS_root() + "train_split_" + str(
                    part.split("MUS_")[1]) + ".txt") as tsv:
                train = list(str(system_settings.get_MAPS_root() + line[0].split(".wav")[0]) for line in
                             csv.reader(tsv, dialect="excel-tab"))
            self.train, self.validation = train_test_split(train, test_size=0.1, random_state=3)
            with open(system_settings.get_MAPS_root() + "test_split_" + str(part.split("MUS_")[1]) + ".txt") as tsv:
                self.test = list(str(system_settings.get_MAPS_root() + line[0].split(".wav")[0]) for line in
                                 csv.reader(tsv, dialect="excel-tab"))
            print("[DATA] gathered files")
        else:
            for root, dirnames, filenames in os.walk(system_settings.get_MAPS_root()):
                for filename in fnmatch.filter(filenames, "*.wav"):
                    matches.append(os.path.join(root, filename))
            part_matches = list(match.replace(".wav", "") for match in matches if part in match)
            print("[DATA] gathered files")
            # random state and order or for making sure the train,validation and testset are the same for the gridsearches
            part_matches = sorted(part_matches)
            train, self.test = train_test_split(part_matches, random_state=3, test_size=0.2)
            self.train, self.validation = train_test_split(train, test_size=0.2, random_state=3)
        self.excerpt = parse_int_config(load_config, 'MAPS_excerpt')
        self.random_minute = parse_bool_config(load_config,'random_minute')

    def parse_maps_txt(self,txt):
        with open(txt) as tsv:
            return list(line for line in csv.reader(tsv, dialect="excel-tab"))[1:]

    def process_maps_file(self, filename, verbose=False, ext=".wav"):
        '''
        Audio file frame generator for MAPS dataset

        @param filename: name of the file
        @extension : the extension of the audiofile
        @param excerpt: if not None (default = None), a tuple (begin,end) denotes
        to use only the excerpt from second begin till second end
        '''
        if verbose:
            print(filename)

        local_path = filename + ext
        txt_path = filename + ".txt"

        # extract audio
        [fs, audio_stereo] = wavfile.read(local_path)
        Xs = np.mean(audio_stereo, axis=1)  # audio_stereo[:,0]

        start = 0
        # excerpt
        if (not self.excerpt is None):
            if (not (self.random_minute)):
                Xs = Xs[0: self.excerpt * fs]
            else:
                if (len(Xs) > (self.excerpt * fs)):
                    start = random.randint(0, len(Xs) - (self.excerpt * fs))
                Xs = Xs[start:start + (self.excerpt * fs)]
        # extract targets
        Ys = self.parse_maps_txt(txt_path)
        # print("music file, #notes: %s" % (len(Ys)))
        return (Xs, Ys, filename, fs, start)

    def get_train_generator(self):
        for match in self.train:
            yield self.process_maps_file(match)
    def get_validation_generator(self):
        for match in self.validation:
            yield self.process_maps_file(match)
    def get_test_generator(self):
        for match in self.test:
            yield self.process_maps_file(match)


class MusicNet_generators:
    def __init__(self, part, load_config):
        global system_config
        matches = []
        print("[DATA] loading MusicNet files...")
        self.data = np.load(open(system_settings.get_MusicNet_root() + 'musicnet.npz', 'rb'))
        print("[DATA] gathered files")
        keys = []
        random_state=5
        if part != "MUS":
            metadata = pd.read_csv(system_settings.get_MusicNet_root() + "musicnet_metadata.csv", 'wb', delimiter=',')
            if not(part in ['SP_1','SP_2','SP_3','SP_4']):
                keys = metadata.loc[metadata["ensemble"] == part]["id"].values.tolist()
            else:
                keys = metadata.loc[metadata["ensemble"] == "Solo Piano"][
                    "id"].values.tolist()
                random_state= int(part[3])
                print random_state
            keys = [str(key) for key in keys]
        else:
            keys = self.data.keys()
        keys = sorted(keys)
        train, self.test = train_test_split(keys, test_size=0.2, random_state=random_state)
        self.train, self.validation = train_test_split(train, test_size=0.2, random_state=random_state)
        self.excerpt = parse_int_config(load_config, 'MusicNet_excerpt')
        self.random_minute = parse_bool_config(load_config,'random_minute')

    def parse_MusicNet_intervaltree(self, Y):
        fs = 44100
        return list(
            (float(start) / fs, float(end) / fs, note) for (start, end, (instrument, note, measure, beat, note_value))
            in sorted(Y))

    def process_file(self, filename, verbose=False, excerpt=(0, 150)):
        '''
        Audio file frame generator for MusicNet dataset

        @param filename: name of the file
        @param excerpt: if not None (default = None), a tuple (begin,end) denotes
        to use only the excerpt from second begin till second end
        '''
        if verbose:
            print(filename)
        fs = 44100

        # extract audio
        Xs, Y = self.data[filename]
        start = 0
        # excerpt
        if (not self.excerpt is None):
            if (not (self.random_minute)):
                Xs = Xs[0: self.excerpt * fs]
            else:
                if (len(Xs) > (self.excerpt * fs)):
                    start = random.randint(0, len(Xs) - (self.excerpt * fs))
                Xs = Xs[start:start + (self.excerpt * fs)]
        # extract targets
        Ys = self.parse_MusicNet_intervaltree(Y)
        # print("music file, #notes: %s" % (len(Ys)))
        Xs = Xs * 28000  # 28000 is het maximum en -28000 het minimum van de maps dataset. Deze waarden zijn genormaliseerd en dat verpest de log van de spectograms
        return (Xs, Ys, filename, fs, start)

    def get_train_generator(self):
        for match in self.train:
            yield self.process_file(match)

    def get_validation_generator(self):
        for match in self.validation:
            yield self.process_file(match)

    def get_test_generator(self):
        for match in self.test:
            yield self.process_file(match)
