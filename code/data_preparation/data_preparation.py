"""
Created : December 24, 2016

Author: Hannes Buseyne
"""
#
import os

import numpy as np
import system_settings
from configs import parse_int_config
from configs import parse_bool_config

from qspectro import generate_qgram
from spectro import generate_spectrogram


class DataPreparation:
    def __init__(self, prep_conf, load_conf, label_generation):
        self.fft_nperseg = int(prep_conf['frame_length'])
        self.fft_noverlap = self.fft_nperseg // 4
        self.transform = prep_conf['transform']
        self.cutoff = parse_int_config(prep_conf, 'cutoff')
        self.spec_dim = int(prep_conf['spec_dim'])
        self.filter_useful = parse_bool_config(prep_conf,'filter_useful')
        self.label_generation = label_generation
        self.slice_length = int(prep_conf['slice'])
        self.use_gradient = parse_bool_config(prep_conf,'use_gradient')
        self.load_conf = load_conf


    def transform_signal(self, Xs, fs):
        # transform to frequency domain
        (t, f, Sxx) = (0, 0, 0)
        if self.transform == 'fft':
            (t, f, Sxx) = generate_spectrogram(Xs, fs, nperseg=self.fft_nperseg)
        elif self.transform == 'cqt':
            (t, f, Sxx) = generate_qgram(Xs, fs, nperseg=self.slice_length, bins=self.spec_dim * 12)
        elif self.transform == None:
            frames = np.zeros(shape=(len(list(Xs[i:i + self.slice_length].tolist() for i in
                                              range(0, len(Xs) - self.slice_length, self.slice_length / 4))),
                                     self.slice_length))
            j = 0
            for i in range(0, len(Xs) - self.slice_length, self.slice_length / 4):
                frames[j, :] = Xs[i:i + self.slice_length]
                j += 1
            (t, f, Sxx) = (np.linspace(0, len(Xs) / fs, frames.shape[0]), fs, np.transpose(frames))

        # drop frequencies beyond cutoff
        if self.cutoff is not None:
            ci = np.searchsorted(f, self.cutoff)
            (f, Sxx) = (f[0:ci], Sxx[0:ci, :])

        return (t, f, Sxx)

    def yield_spectrum_from_audio(self, Xs, fs, Ys, note_name, start):
        '''
        Generates spectrum frames for a given time signal and target information

        @param Xs: time signal
        @param fs: sampling frequency
        @param Ys: targets
        @param note_name: origin
        '''

        (t, f, Sxx) = self.transform_signal(Xs, fs)
        # divide in frames and yield
        for i in range(0, Sxx.shape[1] - 1):
            # time and spectrum frame
            Sxx_frame = Sxx[:, i]
            t_frame = t[i] + float(start) / fs

            targets = []
            # targets
            if Ys is not None:
                targets = self.label_generation.get_targets(Sxx_frame, Ys, t_frame)

            # yield under conditions
            if (not self.filter_useful) or self.useful(targets):
                yield (f, Sxx_frame, targets, note_name + " - Frame - [" + str(i) + "]")

    def useful(self, targets):
        """
        a frame is 'useful' is it has labels, so if there is a note present in the frame
        :param targets: the labels for the frame
        :return: True if there are targets specified in the given targets
        """
        return len(targets) > 0

    def get_number_of_ouputs(self):
        """
        returns the number of labels that has to be predicted
        :return: the number of labels that has to be predicted
        """
        return self.label_generation.get_number_of_outputs()

    def write_to_numpy(self,gen, lmdb_path):
        """
        write the frames produced by the generator to numpy arrays with their labels
        :param gen: the generator producing the frames in (frame,label,name
        :param lmdb_path:
        :return:
        """
        os.makedirs(lmdb_path)
        np_frames = []
        np_targets = []
        ex_nr = 0
        for (xs, ys, name, fs, start) in gen:
            frames = list(frame for frame in self.yield_spectrum_from_audio(xs, fs, ys, name, start))
            (ex_frames, targets) = self.label_generation.get_frame_labels(frames)
            # ex_frames = np.gradient(ex_frames, axis=1)
            np_frames.append(ex_frames)
            np_targets.append(targets)
            if len(np_targets) == 20:
                np.vstack(np_frames).dump(lmdb_path + "examples_" + str(ex_nr) + ".p")
                np.vstack(np_targets).dump(lmdb_path + "labels_" + str(ex_nr) + ".p")
                np_frames = []
                np_targets = []
                ex_nr += 1
        np.vstack(np_frames).dump(lmdb_path + "examples_" + str(ex_nr) + ".p")
        np.vstack(np_targets).dump(lmdb_path + "labels_" + str(ex_nr) + ".p")

    def save_numpy(self, train_gen, val_gen, test_gen, data_source, part, data_preparation):
        """
        saves data from generator to numpy arrays, this makes an folder for the training data, validation data and test data
        :param generator: the generator to get the data samples from
        """
        #define lmdb_pathss
        print("[DATA] saving training data...")
        train_path = system_settings.get_data_path(data_source, part, data_preparation) + "/train/"
        self.write_to_numpy(train_gen,train_path)
        print("[DATA] saving validation data...")
        validation_path = system_settings.get_data_path(data_source, part, data_preparation) + "/validation/"
        self.write_to_numpy(val_gen, validation_path)
        print("[DATA] saving test data...")
        test_path = system_settings.get_data_path(data_source, part, data_preparation) + "/test/"
        self.write_to_numpy(test_gen, test_path)

    def get_path(self):
        if self.transform == 'cqt':
            return "TF_" + str(self.transform) + "_labels_" + str(
                self.label_generation.get_label_type()) + "_FL_" + str(
                self.slice_length) + "_SD_" + str(self.spec_dim)
        else:
            return "TF_" + str(self.transform) + "_labels_" + str(
                self.label_generation.get_label_type()) + "_FL_" + str(
                self.fft_nperseg) + "_SD_" + str(self.spec_dim)

    def get_load_config(self):
        return self.load_conf
