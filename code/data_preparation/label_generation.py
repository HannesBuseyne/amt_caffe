"""
Created : March 22, 2016

Author: Hannes Buseyne
"""
import numpy as np


class LabelGeneration:
    """
    This is an interface for label generation
    """

    def get_targets(self, Xs, Ys, t):
        """
        get the targets for the given frame at the given time
        :param Xs: the audio sample
        :param Ys: the label sample in format [start_time,end_time,note_number]
        :param t: the timing in the frame
        :return:
        """
        raise Exception("should be overwritten")

    def get_number_of_outputs(self):
        """
        return the number of outputs to be predicted (the number of labels)
        """
        raise Exception("should be overwritten")

    def get_frame_labels(self, frames):
        """
        return the a tupel of two numpy arrays (frames,labels)
        """
        raise Exception("should be overwritten")

    def get_label_type(self):
        """
        return the name of the label type as a string
        """
        raise Exception("should be overwritten")


class AttentionLabelGeneration(LabelGeneration):
    """
    This class generates the labels for models where ground frequencies are predicted
    """

    def __init__(self, spec_dim):
        self.spec_dim = int(spec_dim)

    def get_targets(self, Xs, Ys, t):
        notelabels = list((int(Y[2]) - 21) * self.spec_dim + (self.spec_dim * 2 - 1) for Y in Ys if
                          np.float(Y[0]) <= t and np.float(Y[1]) >= t)
        return_np = np.zeros(shape=Xs.shape)
        for indices in list(range(x - (self.spec_dim / 2), x + (self.spec_dim / 2)) for x in notelabels):
            return_np[indices] = Xs[indices]
        return return_np

    def get_number_of_outputs(self):
        if self.spec_dim < 8:
            return (112 * self.spec_dim) + 1
        else:
            return 112 * self.spec_dim

    def get_frame_labels(self, frames):
        nb_of_examples = len(frames)
        (_, notes, Y, name) = frames[0]
        targets = np.zeros(shape=(nb_of_examples, Y.shape[0]), dtype=np.float64)
        ex_frames = np.zeros(shape=(nb_of_examples, notes.shape[0]), dtype=np.float64)
        for index, frame in enumerate(frames):
            (_, notes, Y, name) = frame
            targets[index, :] = Y
            ex_frames[index, :] = notes
        return (ex_frames, targets)

    def get_label_type(self):
        return "attention"


class NoteLabelgeneration(LabelGeneration):
    """
    This class generates the labels for the models where notes are predicted
    """

    def __init__(self):
        self.nb_of_notes = 88

    def get_targets(self, Xs, Ys, t):
        return list(int(Y[2]) for Y in Ys if np.float(Y[0]) <= t and np.float(Y[1]) >= t)

    def get_number_of_outputs(self):
        return self.nb_of_notes

    def get_frame_labels(self, frames):
        nb_of_examples = len(frames)
        (_, notes, Y, name) = frames[0]
        targets = np.zeros(shape=(nb_of_examples, self.nb_of_notes), dtype=np.float64)
        ex_frames = np.zeros(shape=(nb_of_examples, notes.shape[0]), dtype=np.float64)
        for index, frame in enumerate(frames):
            (_, notes, Y, name) = frame
            for y in Y:
                targets[index, y - 21] = 1
            ex_frames[index, :] = notes
        return (ex_frames, targets)

    def get_label_type(self):
        return "notes"


class AutoEncoderLabelGeneration(LabelGeneration):
    """
    This class generates the labels for the autoencoder (thus it predicts the input)
    """

    def __init__(self, spec_dim):
        self.spec_dim = int(spec_dim)

    def get_targets(self, Xs, Ys, t):
        return Xs

    def get_number_of_outputs(self):
        if self.spec_dim < 8:
            return (112 * self.spec_dim) + 1
        else:
            return 112 * self.spec_dim

    def get_frame_labels(self, frames):
        nb_of_examples = len(frames)
        (_, notes, Y, name) = frames[0]
        ex_frames = np.zeros(shape=(nb_of_examples, notes.shape[0]), dtype=np.float64)
        for index, frame in enumerate(frames):
            (_, notes, Y, name) = frame
            ex_frames[index, :] = notes
        return (ex_frames, ex_frames)

    def get_label_type(self):
        return "auto_encoder"
