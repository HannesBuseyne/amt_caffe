"""
Created : December 24, 2016

Author: Hannes Buseyne
"""

import numpy as np
from configs import parse_bool_config


class PostProcessor:
    """
    Interface for postprocessor
    """

    def __init__(self, config):
        self.threshold = float(config["threshold"])
        self.moving_threshold = parse_bool_config(config,"moving_threshold")
        if self.moving_threshold:
            self.min_threshold = float(config['min_threshold'])
            if config['moving_method'] in ['lin', 'lin_upper_notes', 'exp', 'step']:
                self.moving_method = config['moving_method']
            else:
                raise Exception("threshold moving method not supported")
            self.lin_step = float(config['lin_step'])
            self.exp_factor = float(config['exp_factor'])

    def get_note_statistics(self, gt, ests):
        """
        returns the note statistics, a tuple with arrays pointing to respectively false positives, false negatives, true positives and true negatives
        :param gt: the ground truth : the label for this batch
        :param ests: the score as predicted by the network for this batch
        :param threshold: the threshold to predict 'TRUE'
        :param moving_threshold: boolean : is the moving threshold used or not (see thesis)
        :return: a tuple of np arrays of the same dimensions as gt and ests that have 1 for respectively false negatives, false positives, true negatives, and true positives)
        """
        test_border_gt = 0.02
        ests = self.get_note_ests(ests)
        gt = np.greater(self.get_note_ests(gt), np.full(ests.shape, test_border_gt))
        if not (self.moving_threshold):
            false_negatives = np.logical_and(np.greater(np.full(gt.shape, self.threshold), ests),
                                             np.greater(gt, np.full(gt.shape, test_border_gt)))
            false_positives = np.logical_and(np.greater(ests, np.full(gt.shape, self.threshold)),
                                             np.greater(np.full(gt.shape, test_border_gt), gt))
            true_positives = np.logical_and(np.greater(ests, np.full(gt.shape, self.threshold)),
                                            np.greater(gt, np.full(gt.shape, test_border_gt)))
            true_negatives = np.logical_and(np.greater(np.full(gt.shape, self.threshold), ests),
                                            np.greater(np.full(gt.shape, test_border_gt), gt))

        else:
            false_negatives = np.empty(shape=gt.shape, dtype=np.bool)
            false_positives = np.empty(shape=gt.shape, dtype=np.bool)
            true_negatives = np.empty(shape=gt.shape, dtype=np.bool)
            true_positives = np.empty(shape=gt.shape, dtype=np.bool)
            thresholds = np.empty(shape=(1, gt.shape[1]))
            thresholds.fill(self.threshold)
            for i in range(gt.shape[0]):
                # print threshold
                false_negatives[i, :] = np.logical_and(np.greater(thresholds, ests[i, :]),
                                                       np.greater(gt[i, :],
                                                                  np.full(gt.shape, test_border_gt)[i, :]))
                false_positives[i, :] = np.logical_and(np.greater(ests[i, :], thresholds),
                                                       np.greater(np.full(gt.shape, test_border_gt)[i, :],
                                                                  gt[i, :]))
                true_positives[i, :] = np.logical_and(np.greater(ests[i, :], thresholds),
                                                      np.greater(gt[i, :],
                                                                 np.full(gt.shape, test_border_gt)[i, :]))
                true_negatives[i, :] = np.logical_and(np.greater(thresholds, ests[i, :]),
                                                      np.greater(np.full(gt.shape, test_border_gt)[i, :],
                                                                 gt[i, :]))
                p_indices = np.where(false_positives[i, :] + true_positives[i, :])
                n_indices = np.where(false_negatives[i, :] + true_negatives[i, :])
                thresholds = self.move_threshold(thresholds, p_indices, n_indices)


        return (false_negatives, false_positives, true_negatives, true_positives)

    def get_fault_note_metrics(self, fn, fp, tp):
        """
        get some metrics about the fault notes and where faults are present
        :param fn: numpy array with the false negatives
        :param fp: numpy array with the false positives
        :param tp: numpy array with the true positives
        :return: a tuple containing in order : the number of false negatives directly before true positives, the number
        of false negatives directly after true positives, the number of false positives directly before true positives,
        the number of false positives directly after false positives, the number of true positive notes, the number of
        false positive notes, the number of false negative notes and the number of true postives, false positives and
        false negatives
        """
        fn = np.transpose(fn)
        fp = np.transpose(fp)
        tp = np.transpose(tp)
        (fn_before, fn_after, fp_before, fp_after) = (np.zeros(shape=(fn.shape[0])), np.zeros(shape=(fn.shape[0])), np.zeros(shape=(fn.shape[0])), np.zeros(shape=(fn.shape[0])))
        (tp_len, fp_len, fn_len) = (np.sum(tp), np.sum(fp), np.sum(fn))
        present_notes = np.zeros(shape=(3,fn.shape[0]))
        # print diff_tp
        for i in range(fn.shape[0]):
            # print "note " + str(i)
            # loop over notes
            # print "ok"
            notestats = []
            for j, stat in enumerate([tp, fp, fn]):
                notestats.append(np.split(stat[i, :], np.where(np.diff(stat[i, :]) != 0)[0] + 1))
                lengths = [np.sum(array) for array in notestats[j] if np.sum(array) > 0]
                present_notes[j,i] += len(lengths)
            fn_before[i] += np.where(np.logical_and(np.diff(tp[i, :].astype(int)) + np.diff(fn[i, :].astype(int)) == 0,
                                                 np.diff(tp[i, :].astype(int)) > 0))[0].shape[0]
            fn_after[i] += np.where(np.logical_and(np.diff(tp[i, :].astype(int)) + np.diff(fn[i, :].astype(int)) == 0,
                                                np.diff(tp[i, :].astype(int)) < 0))[0].shape[0]
            fp_before[i] += np.where(np.logical_and(np.diff(tp[i, :].astype(int)) + np.diff(fp[i, :].astype(int)) == 0,
                                                 np.diff(tp[i, :].astype(int)) > 0))[0].shape[0]
            fp_after[i] += np.where(np.logical_and(np.diff(tp[i, :].astype(int)) + np.diff(fp[i, :].astype(int)) == 0,
                                                np.diff(tp[i, :].astype(int)) < 0))[0].shape[0]
        return (
        fn_before, fn_after, fp_before, fp_after, present_notes[0,:], present_notes[1,:], present_notes[2,:], tp_len, fp_len,
        fn_len)

    def get_note_ests(self, ests):
        """
        return the estimated labels in terms of notes
        :param ests: the estimates given in the format normal for the postprocessor
        :return: the estimates in terms of notes
        """
        raise Exception("should be overwritten!")

    def get_predictions(self, ests):
        """
        returns the postive and negative note predictions for a given output of the network
        :param ests: the output of the network
        :return: the postives predicted notes for this network
        """
        ests = self.get_note_ests(ests)
        prediction = np.empty(shape=ests.shape, dtype=np.bool)
        if not (self.moving_threshold):
            return np.greater(ests, np.full(ests.shape, self.threshold))
        else:
            thresholds = np.empty(shape=(1, ests.shape[1]))
            thresholds.fill(self.threshold)
            for i in range(ests.shape[0]):
                prediction[i,:] = np.greater(ests[i,:], thresholds)
                p_indices = np.where(prediction[i,:])
                n_indices = np.where(np.logical_not(prediction[i,:]))
                thresholds = self.move_threshold(thresholds,p_indices,n_indices)
            return prediction

    def check_predictions(self, gt, pred):
        test_border_gt = 0.02
        gt = self.get_note_ests(gt)
        neg = np.logical_not(pred)
        false_negatives = np.logical_and(neg,
                                         np.greater(gt, np.full(gt.shape, test_border_gt)))
        false_positives = np.logical_and(pred,
                                         np.greater(np.full(gt.shape, test_border_gt), gt))
        true_positives = np.logical_and(pred,
                                        np.greater(gt, np.full(gt.shape, test_border_gt)))
        true_negatives = np.logical_and(neg,
                                        np.greater(np.full(gt.shape, test_border_gt), gt))
        return (false_negatives, false_positives, true_negatives, true_positives)

    def move_threshold(self, thresholds, p_indices, n_indices, ):
        """
        moves the threshold according to the given positive and negative indices
        :param thresholds: the previous threshold array
        :param p_indices: the note indices that where predicted positive
        :param n_indices: the note indices that where predicted negative
        :return:
        """
        if self.moving_method == 'lin':
            for j in p_indices[0]:
                if thresholds[0, j] > self.min_threshold:
                    thresholds[0, j] -= self.lin_step
        elif self.moving_method == 'lin_upper_notes':
            p_indices = list(p_index for p_index in p_indices[0] if p_index > 28)
            for j in p_indices:
                if thresholds[0, j] > self.min_threshold:
                    thresholds[0, j] -= self.lin_step
        elif self.moving_method == 'exp':
            thresholds[0, p_indices] =  thresholds[0, p_indices] * 0.9
        elif self.moving_method == 'step':
            thresholds[0, p_indices] = self.min_threshold
        thresholds[0, n_indices] = self.threshold
        return thresholds

    def get_threshold(self):
        return self.threshold

    def set_threshold(self, threshold):
        self.threshold = threshold


class NormalPostprocessor(PostProcessor):
    """
    This is a postprocessor for the case where labels are notes
    """

    def __init__(self, config):
        PostProcessor.__init__(self, config)

    def get_note_ests(self, ests):
        return ests


class AttentionPostprocessor(PostProcessor):
    def __init__(self, config):
        PostProcessor.__init__(self, config)
        self.aggregate = config['attention_aggregate']

    def get_note_ests(self, ests):
        half_spec_dim = int(round(ests.shape[1] / 224))
        return np.hstack(list(
            np.expand_dims(mean, axis=1) for index, mean in
            enumerate(
                list(np.__dict__[self.aggregate](ests[:, range(x - half_spec_dim, x + half_spec_dim)], axis=1) for x in
                     list(i * (half_spec_dim * 2) + (half_spec_dim * 4) - 1 for i in range(0, 88))))))


class AutoEncoderPostprocessor(PostProcessor):
    def __init__(self):
        PostProcessor.__init__(self, {"threshold": 0, "moving_threshold": "False"})

    def get_note_ests(self, ests):
        return ests
