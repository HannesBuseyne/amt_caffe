'''
Created on Feb 16, 2016

@author: Daan Seynaeve
'''
import math
import warnings

import numpy as np
from matplotlib import pyplot as plt
from nsgt import NSGT_sliced, OctScale


def assemble_coeffs(cqt, ncoefs):
    '''
    Build a sequence of blocks out of incoming overlapping CQT slices
    
    based on spectrogram.py example from nsgt toolbox
    '''
    cqt = iter(cqt)
    cqt0 = cqt.next()
    cq0 = np.asarray(cqt0).T
    shh = int(round(cq0.shape[0] // 2))
    # print cq0.shape[0]
    # print int(round(ncoefs))
    out = np.empty((int(round(ncoefs)), cq0.shape[1]), dtype=cq0.dtype)
    # out = np.empty((ncoefs,cq0.shape[1],cq0.shape[2]), dtype=cq0.dtype)

    fr = 0
    sh = max(0, min(shh, int(round(ncoefs)) - fr))
    # print fr
    # print sh
    out[fr:fr + sh] = cq0[sh:]  # store second half
    # add up slices
    for cqi in cqt:
        cqi = np.asarray(cqi).T
        out[fr:fr + sh] += cqi[:sh]
        cqi = cqi[sh:]
        fr += sh
        sh = max(0, min(shh, int(round(ncoefs)) - fr))
        out[fr:fr + sh] = cqi[:sh]

    return out[:fr]


def downpool_coeffs(QSxx, fs, res_fps, coef_factor, pool_func, number_of_coef):
    '''
    Downsample the result signal with a pooling function in time
    
    based on spectrogram.py example from nsgt toolbox
    '''
    m = len(QSxx)
    coefs_per_sec = fs * coef_factor

    dur = m / coefs_per_sec  # final duration of MLS
    pool_factor = int(coefs_per_sec / res_fps + 0.5)  # pooling factor
    pooled_len = QSxx.shape[0] // pool_factor
    dur *= (pooled_len * pool_factor) / float(m)
    QSxx = QSxx[:pooled_len * pool_factor]
    QSxx = QSxx.reshape((pooled_len, pool_factor,) + QSxx.shape[1:])
    difference = QSxx.shape[0] - math.floor(float(number_of_coef) / fs * res_fps)
    if difference > 0:
        drop_per = float(QSxx.shape[0]) / difference
        QSxx = np.delete(QSxx, list(
            round(i * drop_per) for i in range(int(difference + 1)) if round(i * drop_per) < QSxx.shape[0]), axis=0)
    poolfun = np.__dict__[pool_func]
    QSxx = poolfun(QSxx, axis=1)

    return QSxx


def generate_qgram(Xs, fs, bins=6 * 12, fmin=25, fmax=16000, nperseg=2 ** 16, res_fps=25):
    '''
    Generate a CQT spectrogram of Xs based on the sliced NSGT algorithm.
    
    @param Xs: signal (numpy array)
    @param fs: samplerate
    @param bins: frequency bins per octave
    @param fmin: minimum frequency
    @param fmax: maximum frequency
    @param nperseg: the size of the batches on which sliced CQT is applied.
    '''

    warnings.filterwarnings("ignore")
    print Xs
    n = len(Xs)  # signal length
    tr_len = nperseg // 4  # slice overlap
    Qvar = 1  # varies the bandwith: Qfactor' = Qfactor / Qvar

    # build sliced transform
    scale = OctScale(fmin, fmax, bins)
    slicqt = NSGT_sliced(scale, fs=fs, sl_len=nperseg, tr_area=tr_len,
                         real=1, reducedform=1, matrixform=True, Qvar=Qvar)

    # apply transform
    Yiter = slicqt.forward((Xs,))

    # recombine slices
    Qxx = assemble_coeffs(Yiter, slicqt.coef_factor * n)

    # log-magnitude power spectrum
    QSxx = Qxx
    np.absolute(QSxx, QSxx)
    QSxx += 1  # avoid log(0) + fix synthesis
    np.log10(QSxx, QSxx)

    # downpool
    QSxx = downpool_coeffs(QSxx, fs, res_fps, slicqt.coef_factor, 'mean', n)

    # frequency and time axes
    f = scale.F()
    t = np.linspace(0, float(n) / fs, np.shape(QSxx)[0])

    return (t, f, QSxx.T)


def plot_qgram(t, f, QSxx, title):
    '''
    Plot a CQT spectrogram
    '''

    print("plotting CQ-NSGT spectrogram for " + title + " ... ")

    fig = plt.figure(facecolor="#EFEFEF")
    fig.suptitle(title)

    fi = np.arange(0, len(f))  # pcolormesh does not handle the log axis well...
    plt.pcolormesh(t, fi, QSxx, cmap='gray')

    plt.colorbar().set_label('Power/Frequency [dB/Hz]')
    plt.ylabel('Frequency bin index')
    plt.xlabel('Time [sec]')

    plt.axis('tight')
    plt.show()


def generate_and_plot_qgram(Xs, fs, title):
    print fs
    t, f, QSxx = generate_qgram(Xs, fs)
    print(np.shape(QSxx))
    plot_qgram(t, f, QSxx, title)


def generate_and_plot_qgrams(Xs1, Xs2, fs, title, cutoff):
    threshold = 0.3
    spec_dim = 6
    cut_freq = (int(cutoff) - 21) * spec_dim + (spec_dim * 2 - 1) - 3
    print (int(cutoff) - 21) * spec_dim + (spec_dim * 2 - 1) - 3
    t1, f1, QSxx1 = generate_qgram(Xs1, fs)
    QSxx1 = QSxx1 / np.max(QSxx1)
    t2, f2, QSxx2 = generate_qgram(Xs2, fs)
    QSxx2 = QSxx2 / np.max(QSxx2)
    # fig = plt.figure(facecolor="#EFEFEF")
    f, (ax1, ax2, ax3) = plt.subplots(1, 3, sharey=True)
    # fig.suptitle(title)

    fi1 = np.arange(cut_freq, len(f1))  # pcolormesh does not handle the log axis well...
    ax1.pcolormesh(t1, fi1, QSxx1[cut_freq:, :], cmap='Greens')

    fi2 = np.arange(cut_freq, len(f2))  # pcolormesh does not handle the log axis well...
    ax2.pcolormesh(t2, fi2, QSxx2[cut_freq:, :], cmap='Blues')

    QSxx3 = np.logical_and(QSxx1[cut_freq:, :] > threshold, QSxx2[cut_freq:, :] > threshold)
    print "coverage : " + str(float(np.sum(QSxx3)) / np.sum(QSxx2[cut_freq:, :] > threshold))
    print QSxx3.shape

    fi2 = np.arange(cut_freq, len(f2))  # pcolormesh does not handle the log axis well...
    ax3.pcolormesh(t2, fi2, QSxx3, cmap='Blues')
    # ax1.colorbar().set_label('Power/Frequency [dB/Hz]')
    # ax2.colorbar().set_label('Power/Frequency [dB/Hz]')
    plt.ylabel('Frequency bin index')
    plt.xlabel('Time [sec]')

    plt.axis('tight')
    plt.show()
