'''
Created on Feb 16, 2016

@author: Daan Seynaeve
'''
import math
import warnings

import numpy as np
from matplotlib import pyplot as plt
from nsgt import NSGT_sliced, OctScale


def assemble_coeffs(cqt, ncoefs):
    '''
    Build a sequence of blocks out of incoming overlapping CQT slices

    based on spectrogram.py example from nsgt toolbox
    '''
    cqt = iter(cqt)
    cqt0 = cqt.next()
    cq0 = np.asarray(cqt0).T
    shh = cq0.shape[0] // 2
    out = np.empty((ncoefs, cq0.shape[1]), dtype=cq0.dtype)
    # out = np.empty((ncoefs,cq0.shape[1],cq0.shape[2]), dtype=cq0.dtype)

    fr = 0
    sh = max(0, min(shh, ncoefs - fr))
    out[fr:fr + sh] = cq0[sh:]  # store second half
    # add up slices
    for cqi in cqt:
        cqi = np.asarray(cqi).T
        out[fr:fr + sh] += cqi[:sh]
        cqi = cqi[sh:]
        fr += sh
        sh = max(0, min(shh, ncoefs - fr))
        out[fr:fr + sh] = cqi[:sh]

    return out[:fr]


def downpool_coeffs(QSxx, fs, res_fps, coef_factor, pool_func, number_of_coef):
    '''
    Downsample the result signal with a pooling function in time

    based on spectrogram.py example from nsgt toolbox
    '''
    m = len(QSxx)
    coefs_per_sec = fs * coef_factor
    print float(number_of_coef) / fs * coefs_per_sec
    dur = m / coefs_per_sec  # final duration of MLS
    pool_factor = int(coefs_per_sec / res_fps + 0.5)  # pooling factor
    # looks like the shape changes 24 frames over the lentth of 4.7 seconds
    # this needs to be overcome, we need an equal amount of frames per second
    # we can split the frames per second, the fault will be around 6 frames per second with about 250 frames per second
    pooled_len = QSxx.shape[0] // pool_factor
    dur *= (pooled_len * pool_factor) / float(m)
    QSxx = QSxx[:pooled_len * pool_factor]
    QSxx = QSxx.reshape((pooled_len, pool_factor,) + QSxx.shape[1:])
    difference = QSxx.shape[0] - math.floor(float(number_of_coef) / fs * res_fps)
    print difference

    drop_per = float(QSxx.shape[0]) / difference
    QSxx = np.delete(QSxx, list(
        round(i * drop_per) for i in range(int(difference + 1)) if round(i * drop_per) < QSxx.shape[0]), axis=0)
    poolfun = np.__dict__[pool_func]
    QSxx = poolfun(QSxx, axis=1)

    return QSxx


def generate_qgram(Xs, fs, bins=6 * 12, fmin=25, fmax=16000, nperseg=2 ** 16, res_fps=25):
    '''
    Generate a CQT spectrogram of Xs based on the sliced NSGT algorithm.

    @param Xs: signal (numpy array)
    @param fs: samplerate
    @param bins: frequency bins per octave
    @param fmin: minimum frequency
    @param fmax: maximum frequency
    @param sl_len: the size of the batches on which sliced CQT is applied.
    '''

    warnings.filterwarnings("ignore")

    sl_len = nperseg
    tr_len = sl_len // 4  # slice overlap
    Qvar = 1  # varies the bandwith: Qfactor' = Qfactor / Qvar
    # build sliced transform
    scale = OctScale(fmin, fmax, bins)
    padding = 0
    for index, i in enumerate([j * fs / res_fps for j in range(int(res_fps * (len(Xs) - fs) / fs))]):
        n = len(Xs[i:i + sl_len])  # signal length
        slicqt = NSGT_sliced(scale, fs=fs, sl_len=sl_len, tr_area=tr_len,
                             real=1, reducedform=1, matrixform=True, Qvar=Qvar, recwnd=False)

        # apply transform
        Yiter = slicqt.forward((Xs[i:i + sl_len],))

        # recombine slices
        Qxx = assemble_coeffs(Yiter, slicqt.coef_factor * n)

        # log-magnitude power spectrum
        QSxx = Qxx
        np.absolute(QSxx, QSxx)
        QSxx += 1  # avoid log(0) + fix synthesis
        np.log10(QSxx, QSxx)
        paddingzeros = int(index * float(len(Xs) - sl_len) / fs * fs * slicqt.coef_factor / len(
            range(int(res_fps * (len(Xs) - sl_len) / fs))) - padding)
        padding += paddingzeros
        if index == 0:
            transforms = np.expand_dims(QSxx, axis=2)
        else:
            transforms = np.expand_dims(np.min(np.concatenate([np.lib.pad(transforms,
                                                                          ((0, paddingzeros), (0, 0), (0, 0)),
                                                                          'constant',
                                                                          constant_values=(1000000000, 1000000000)),
                                                               np.expand_dims(np.lib.pad(QSxx, (
                                                               (transforms.shape[0] + paddingzeros - QSxx.shape[0], 0),
                                                               (0, 0)), 'constant',
                                                                                         constant_values=(
                                                                                         1000000000, 1000000000)),
                                                                              axis=2)], axis=2), axis=2), axis=2)
    n = len(Xs)
    # downpool
    QSxx = np.squeeze(transforms)
    QSxx = downpool_coeffs(QSxx, fs, res_fps, slicqt.coef_factor, 'mean', n)

    # frequency and time axes
    f = scale.F()
    t = np.linspace(0, float(n) / fs, np.shape(QSxx)[0])

    return (t, f, QSxx.T)


def plot_qgram(t, f, QSxx, title):
    '''
    Plot a CQT spectrogram
    '''

    print("plotting CQ-NSGT spectrogram for " + title + " ... ")

    fig = plt.figure(facecolor="#EFEFEF")
    fig.suptitle(title)

    fi = np.arange(0, len(f))  # pcolormesh does not handle the log axis well...
    plt.pcolormesh(t, fi, QSxx, cmap='gray')

    plt.colorbar().set_label('Power/Frequency [dB/Hz]')
    plt.ylabel('Frequency bin index')
    plt.xlabel('Time [sec]')

    plt.axis('tight')
    plt.show()


def generate_and_plot_qgram(Xs, fs, title):
    t, f, QSxx = generate_qgram(Xs, fs)
    print(np.shape(QSxx))
    plot_qgram(t, f, QSxx, title)
