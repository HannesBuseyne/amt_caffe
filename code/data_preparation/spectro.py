'''
Created on 19 Nov 2015

@author: Daan Seynaeve
'''
# from matplotlib import pyplot as plt
import warnings

import numpy as np
from matplotlib import pyplot as plt
from scipy import signal

fft_nperseg = 2048
overlap_factor = 4


def get_fft_f_scale(fs, nperseg=fft_nperseg):
    return np.linspace(0, fs / 2, (fft_nperseg / 2) + 1)


def get_fft_t_scale(fs, nseg, nperseg=fft_nperseg):
    npoints = (nseg * nperseg) - ((nseg - 1) * nperseg // overlap_factor)
    return np.linspace(0, npoints * fs, nseg)


def generate_spectrogram(Xs, fs, nperseg=fft_nperseg):
    '''
    obtain log power spectrogram
    '''

    noverlap = nperseg // overlap_factor  # window overlap

    nfft = nperseg

    amp = 10e3  # amplification
    f, t, Pxx = signal.spectrogram(Xs * amp, fs, window='hanning', nperseg=nperseg, noverlap=noverlap, nfft=nfft)

    Sxx = np.log10(Pxx + 1)
    return (t, f, Sxx)


def plot_spectrogram(t, f, Sxx, title):
    '''
    plot a spectrogram with given time axis, frequency axis, log power matrix and plot title
    '''

    warnings.filterwarnings("ignore")

    print("plotting wavspec for " + title + " ... ")
    fig = plt.figure(facecolor="#EFEFEF")
    fig.suptitle(title)

    plt.pcolormesh(t, f, Sxx, cmap='gray')

    plt.axis('tight')
    plt.ylim([0, 8000])
    plt.colorbar().set_label('Power/Frequency [dB/Hz]')
    plt.ylabel('Frequency [Hz]')
    plt.xlabel('Time [sec]')
    plt.hlines(y=440, xmin=0, xmax=4, color='r')
    # print("drawing line at " + str(440*math.pow(2,(1.0/4.0))))
    # print("drawing line at " +str(440.0*math.pow((1.0/2.0),(1.0/3.0))))
    # plt.hlines(y=440.0*math.pow((1.0/2.0),(1.0/3.0)), xmin=0, xmax=4, color='b')
    # plt.hlines(y=440*math.pow(2,(1.0/4.0)), xmin=0, xmax=4, color='y')

    plt.show()

    warnings.filterwarnings("default")

    return (t, f, Sxx)


def generate_and_plot_spectrogram(Xs, fs, title):
    t, f, Sxx = generate_spectrogram(Xs, fs)
    print(np.shape(Sxx))
    print Sxx
    plot_spectrogram(t, f, Sxx, title)
