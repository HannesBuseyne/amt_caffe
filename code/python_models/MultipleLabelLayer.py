# imports
import caffe

import numpy as np
from data_loader import NumpyData


class MultipleLabelLayer(caffe.Layer):
    """
    This is a simple synchronous datalayer for training a multilabel model in caffe. It is modeled after the layer used in the ipython examples on caffe
    """

    def setup(self, bottom, top):
        self.top_names = ['data', 'label']

        # === Read input parameters ===

        # params is a python dictionary with layer parameters.
        params = eval(self.param_str)

        # store input as class variables
        self.batch_size = params['batch_size']
        self.preprocessor = "framepreprocessor"
        if "preprocessor" in params:
            self.preprocessor=params["preprocessor"]

        # Create a batch loader to load the images.
        self.data_loader = NumpyData(params["phase"], params["data_path"],self.preprocessor)
        if "iterations" in params and params["iterations"] is not None:
            self.iterations = int(params["iterations"])
        else:
            self.iterations = None

        # === reshape tops ===
        # since we use a fixed input image size, we can shape the data layer
        # once. Else, we'd have to do it in the reshape call.
        top[0].reshape(
            self.batch_size, 1, 1, self.data_loader.get_frame_width())
        # Note the 20 channels (because PASCAL has 20 classes.)
        top[1].reshape(self.batch_size, self.data_loader.get_label_width())

    def forward(self, bottom, top):
        """
        Load data.
        """
        data, labels = self.data_loader.get_data(self.batch_size)
        top[0].data[...] = data
        top[1].data[...] = labels
        if self.iterations is not None:
            self.iterations -= 1
            # This is a hack to unload the data from memory when done
            if self.iterations == 0:
                self.data_loader = None

    def reshape(self, bottom, top):
        """
        There is no need to reshape the data, since the input is of fixed size
        (rows and columns)
        """
        pass

    def backward(self, top, propagate_down, bottom):
        """
        These layers does not back propagate
        """
        pass


def shuffle_in_unison(a, b):
    rng_state = np.random.get_state()
    np.random.shuffle(a)
    np.random.set_state(rng_state)
    np.random.shuffle(b)
