""""
Created : April 1, 2016

Author: Hannes Buseyne
"""

import caffe
from caffe import layers as L

from model import Model


class AutoEncoderModel(Model):
    def __init__(self, solver, data_source, data_part, data_preparation, post_processor):
        Model.__init__(self, "autoencoder", solver, data_source=data_source, data_part=data_part,
                       data_preparation=data_preparation, post_processor=post_processor)

    def build_net(self, data_layer_params):
        # setup the python data layer
        n = caffe.NetSpec()
        n.data, n.label = L.Python(module='MultipleLabelLayer', layer="MultipleLabelLayer",
                                   ntop=2, param_str=str(data_layer_params))

        # the net itself
        n.flatdata = L.Flatten(n.label)
        n.encode1 = L.InnerProduct(n.data, num_output=1000, weight_filler=dict(type='gaussian', std=1, sparse=15),
                                   bias_filler=dict(type='constant', value=0))
        n.encode1neuron = L.Sigmoid(n.encode1)
        n.encode2 = L.InnerProduct(n.encode1neuron, num_output=500,
                                   weight_filler=dict(type='gaussian', std=1, sparse=15),
                                   bias_filler=dict(type='constant', value=0))
        n.encode2neuron = L.Sigmoid(n.encode2)
        n.encode3 = L.InnerProduct(n.encode2neuron, num_output=250,
                                   weight_filler=dict(type='gaussian', std=1, sparse=15),
                                   bias_filler=dict(type='constant', value=0))
        n.encode3neuron = L.Sigmoid(n.encode3)
        n.encode4 = L.InnerProduct(n.encode3neuron, num_output=88,
                                   weight_filler=dict(type='gaussian', std=1, sparse=15),
                                   bias_filler=dict(type='constant', value=0))
        n.decode4 = L.InnerProduct(n.encode4, num_output=250,
                                   weight_filler=dict(type='gaussian', std=1, sparse=15),
                                   bias_filler=dict(type='constant', value=0))
        n.decode4neuron = L.Sigmoid(n.decode4)
        n.decode3 = L.InnerProduct(n.decode4neuron, num_output=500,
                                   weight_filler=dict(type='gaussian', std=1, sparse=15),
                                   bias_filler=dict(type='constant', value=0))
        n.decode3neuron = L.Sigmoid(n.decode3)
        n.decode2 = L.InnerProduct(n.decode3neuron, num_output=1000,
                                   weight_filler=dict(type='gaussian', std=1, sparse=15),
                                   bias_filler=dict(type='constant', value=0))
        n.decode2neuron = L.Sigmoid(n.decode2)
        n.decode1 = L.InnerProduct(n.decode2neuron, num_output=self.get_number_of_outputs(),
                                   weight_filler=dict(type='gaussian', std=1, sparse=15),
                                   bias_filler=dict(type='constant', value=0))
        n.loss = L.SigmoidCrossEntropyLoss(n.decode1, n.flatdata, loss_weight=1)
        n.score = L.Sigmoid(n.decode1)
        n.error = L.EuclideanLoss(n.flatdata, n.score, loss_weight=0)

        return str(n.to_proto())
