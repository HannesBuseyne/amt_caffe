"""
Created : January 15, 2016

Author: Hannes Buseyne
"""

import os

import caffe
import configs
import numpy as np
import system_settings

from model import Model
from model_logging import check_faults_multiple_models
from visualisation import plot_weightmatrix, plot_weightmatrix_color


class CombinedModel(Model):
    """
    This model combines multiple models and assigns them weights and different thresholds
    """
    def __init__(self, model_name, models, weights, combined_threshold):
        self.model_name = "combined_model_" + model_name
        self.models = models
        self.weights = weights
        self.combined_threshold = combined_threshold

    def train(self,stats):
        for model in self.models:
            path = system_settings.get_snapshot_path(model.model_name) + '_iter_' + str(
                model.solver.get_max_iteration()) + '.caffemodel'
            if not os.path.isfile(path):
                model.train(stats)

    def test(self, statistics):
        print("[TESTING] testing the trained model...")
        if system_settings.use_gpu():
            caffe.set_mode_gpu()
            caffe.set_device(0)
        data = str(system_settings.get_data_path(self.models[-1].data_source, self.models[-1].part,
                                                 self.models[-1].data_preparation)) + "/" + str(
            "test") + "/"
        examples_shape = np.vstack([np.load(data + "labels_" + file) for file in
                                    [file.split("examples_")[1] for file in
                                     os.listdir(data) if
                                     file.startswith("examples_")]]).shape
        nb_iterations = int(round(examples_shape[0] / self.models[-1].test_batchsize))
        nets = list(model.get_net(phase="test", iterations=nb_iterations) for model in self.models)
        postprocessors = list(model.get_post_processor() for model in self.models)
        if not os.path.exists(system_settings.get_net_stats_path("combined_model_" +self.model_name)):
            os.makedirs(system_settings.get_net_stats_path("combined_model_" +self.model_name))
        check_faults_multiple_models("combined_model_" +self.model_name, nets, nb_iterations,
                     postprocessors,self.weights,self.combined_threshold,
                     statistics)
        statistics.write_to_csv()
        print("[TESTING] testing done...")

    def visualize(self, config=configs.get_defaultconf()['visualization']):
        print("[VISUALISATION] visualising examples...")
        examples_root = system_settings.get_visualisation_path(
            self.model_name) + config['examples_root'] + "/"
        print("[VISUALISATION] visualising the trained model...")
        if system_settings.use_gpu():
            caffe.set_mode_gpu()
            caffe.set_device(0)
        nets = list(model.get_net(phase="test") for model in self.models)
        postprocessors = list(model.get_post_processor() for model in self.models)
        for i in range(int(config['visualized_examples'])):
            if not os.path.exists(examples_root + str(i) + '/'):
                os.makedirs(examples_root + str(i) + '/')
            for net in nets:
                net.forward()
            gt = nets[0].blobs['label'].data  # ground truth
            predictions = list(postprocessor.get_predictions(net.blobs['score'].data) for (postprocessor, net) in
                               zip(postprocessors, nets))  # est
            prediction = np.zeros(shape=predictions[0].shape)
            for index, pred in enumerate(predictions):
                plot_weightmatrix(np.transpose(pred), title="prediction model " + str(index), savefolder=examples_root + str(i) + '/')
                prediction += (self.weights[index] * np.pred)
            prediction = np.greater(prediction, np.full(prediction.shape, self.combined_threshold))
            (fn, fp, tn, tp) \
                = postprocessors[0].check_predictions(gt, prediction)
            plot_weightmatrix(np.transpose(prediction),
                              title="prediction_without_threshold", savefolder=examples_root + str(i) + '/')
            plot_weightmatrix(np.transpose(fn), title="false negatives", savefolder=examples_root + str(i) + '/')
            plot_weightmatrix(np.transpose(fp), title="false positives", savefolder=examples_root + str(i) + '/')
            plot_weightmatrix(np.transpose(tn), title="true negatives", savefolder=examples_root + str(i) + '/')
            plot_weightmatrix(np.transpose(tp), title="true positives", savefolder=examples_root + str(i) + '/')
            plot_weightmatrix(np.transpose(tp + fn), title="label_notes", savefolder=examples_root + str(i) + '/')
            plot_weightmatrix(np.transpose(tp + fp), title="prediction", savefolder=examples_root + str(i) + '/')
            plot_weightmatrix_color(
                np.transpose((10 * tp.astype(int)) - (5 * fn.astype(int)) - (10 * fp.astype(int))),
                title="prediction_colored", savefolder=examples_root + str(i) + '/')
            colored = (10 * tp.astype(int)) - (5 * fn.astype(int)) - (
            10 * fp.astype(int))
            with open(examples_root + str(i) + '/prediction_colored.dat','w+') as datafile:
                for x in range(colored.shape[0]):
                    for y in range(colored.shape[1]):
                        datafile.write(str(x) + ' ' + str(y) + ' ' + str(colored[x,y]) + '\n')
                    datafile.write('\n')

        print("[VISUALISATION] visualised examples")

    def make_precision_recall(self, teststats):
        for i in [0.05,0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9,0.95]:
            self.models[-1].post_processor.set_threshold(i)
            self.test(teststats)