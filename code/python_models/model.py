""""
Created : December 25, 2016

Author: Hannes Buseyne
"""
import os

import caffe
import configs
import numpy as np
import system_settings
from data_loader import prepare_data
from log_plotter import plot_fault_analysis, plot_training_curve

from model_logging import check_accuracy, check_faults
from visualisation import plot_1d_frame, plot_weightmatrix, plot_weightmatrix_color,oned_frame_to_pgfplots,weightmatrix_to_pgfplots


class Model:
    def __init__(self, model_name, solver, data_source, data_part,
                 data_preparation, post_processor,preprocessor):
        self.model_name = model_name + "_DS_" + data_source + "-" + str(data_part) + "_" + data_preparation.get_path()
        self.solver = solver
        solver.set_model_name(self.model_name)
        solver.make_solver()
        self.part = data_part
        self.data_preparation = data_preparation
        self.data_source = data_source
        self.post_processor = post_processor
        self.preprocessor = preprocessor
        self.test_batchsize = 128

    def get_post_processor(self):
        """
        returns the postprocessor for the data used by this model
        :return: a postprocessor object describing the postprocessor used for this model
        """
        return self.post_processor

    def get_number_of_outputs(self):
        """
        get the number of output nodes of the model
        :return: the number of output nodes for the model
        """
        return self.data_preparation.get_number_of_ouputs()

    def build_net(self, data_layer_params):
        raise Exception("should be overwritten")

    def get_net(self, phase="train", iterations=None):
        """
        return the neural net for the specific phase and write the specifications for the net to the model folder
        :param phase: either 'validation','test' or 'train', the phase the network is in
        :return: the caffe.Net object representing the net.
        """
        net_name = "trainnet"
        if phase == "validation":
            net_name = "valnet"
        elif phase == "test":
            net_name = "testnet"
        with open(system_settings.get_net_path(self.model_name) + net_name + '.prototxt', 'w') as f:
            f.write(self.build_net(data_layer_params=dict(batch_size=self.test_batchsize, phase=phase, iterations=iterations,
                                                          data_path=system_settings.get_data_path(self.data_source,
                                                                                                  self.part,
                                                                                                  self.data_preparation),
                                                          preprocessor=self.preprocessor)))
        return caffe.Net(system_settings.get_net_path(self.model_name) + net_name + '.prototxt',
                         system_settings.get_snapshot_path(self.model_name) + '_iter_' + str(
                             self.solver.get_max_iteration()) + '.caffemodel',
                         caffe.TEST)

    def train(self, statistics):
        """
        trains the model
        :param statistics: the data object in which you want to save the training statistics.
        :return:
        """
        prepare_data(self.data_source, self.part, self.data_preparation)
        print("[TRAINING] training model...")
        if system_settings.use_gpu():
            caffe.set_mode_gpu()
            caffe.set_device(0)
        with open(system_settings.get_net_path(self.model_name) + 'trainnet.prototxt', 'w') as f:
            f.write(self.build_net(data_layer_params=dict(batch_size=self.solver.get_batch_size(), phase='train',
                                                          data_path=system_settings.get_data_path(self.data_source,
                                                                                                  self.part,
                                                                                                  self.data_preparation),preprocessor=self.preprocessor)))
        with open(system_settings.get_net_path(self.model_name) + 'valnet.prototxt', 'w') as f:
            f.write(self.build_net(data_layer_params=dict(batch_size=self.solver.get_batch_size(), phase='validation',
                                                          data_path=system_settings.get_data_path(self.data_source,
                                                                                                  self.part,
                                                                                                  self.data_preparation),preprocessor=self.preprocessor)))
        net_solver = self.solver.get_solver()
        net_solver.test_nets[0].share_with(net_solver.net)
        for itt in range(self.solver.get_number_of_steps()):
            net_solver.step(self.solver.get_step_size()-20)
            loss = 0
            for i in range(20):
                loss+= net_solver.net.blobs["loss"].data
                net_solver.step(1)
            loss = float(loss)/20
            print loss
            check_accuracy(self.model_name, net_solver.test_nets[0], self.solver.get_test_iterations(),
                           (itt + 1) * self.solver.get_step_size(), self.post_processor,
                           statistics, loss)
        statistics.write_to_csv()
        print("[TESTING] training...")

    def test(self, statistics):
        print("[TESTING] testing the trained model...")
        data = str(system_settings.get_data_path(self.data_source, self.part, self.data_preparation)) + "/" + str("test") + "/"
        examples_shape = np.vstack([np.load(data + "labels_" + file) for file in [file.split("examples_")[1] for file in os.listdir(data) if file.startswith("examples_")]]).shape
        nb_iterations = int(round(examples_shape[0]/self.test_batchsize))
        print "[TESTING] testing " + str(nb_iterations) + " iterations with batchsize " + str(self.test_batchsize) + " for " + str(examples_shape[0]) + " examples"
        if system_settings.use_gpu():
            caffe.set_mode_gpu()
            caffe.set_device(0)
        net = self.get_net(phase="test", iterations=nb_iterations)
        check_faults(self.model_name, net, nb_iterations,
                     self.post_processor,
                     statistics)
        statistics.write_to_csv()
        print("[TESTING] testing done...")

    def visualize(self, config=configs.get_defaultconf()['visualization']):
        print("[VISUALISATION] visualising the trained model...")
        if system_settings.use_gpu():
            caffe.set_mode_gpu()
            caffe.set_device(0)
        net = self.get_net(phase="test")
        fc_params = {pr: (net.params[pr][0].data, net.params[pr][1].data) for pr in net.params}
        print("[VISUALISATION] visualising weights...")
        if not os.path.exists(system_settings.get_visualisation_path(self.model_name) + config['examples_root'] + "/weights/"):
            os.makedirs(system_settings.get_visualisation_path(self.model_name) + config['examples_root'] + "/weights/")
        weights_root = system_settings.get_visualisation_path(self.model_name) + config['examples_root'] + "/weights/"
        for fc in net.params:
            print '[VISUALISATION] {} weights are {} dimensional and biases are {} dimensional'.format(fc,
                                                                                                       fc_params[fc][
                                                                                                           0].shape,
                                                                                                       fc_params[fc][
                                                                                                           1].shape)
            weights = np.squeeze(fc_params[fc][0])
            if len(weights.shape) == 1:
                plot_1d_frame(weights, title="weights_" + fc, savefolder=weights_root)
            elif len(weights.shape) == 2:
                plot_weightmatrix(weights, title="weights_" + fc, savefolder=weights_root)
                weightmatrix_to_pgfplots(weights, title="weights_" + fc, savefolder=weights_root)
                if weights.shape[0] < 10:
                    for index,channel in enumerate(weights):
                        plot_1d_frame(channel, title="weights_" + fc + "_filter_"+str(index),
                                      savefolder=weights_root)
                        oned_frame_to_pgfplots(channel,title="weights_" + fc + "_filter_"+str(index),
                                      savefolder=weights_root)
            elif len(weights.shape) == 3:
                for index in range(weights.shape[1]):
                    channel = np.squeeze(weights[:,index,:])
                    plot_weightmatrix(channel, title="weights_" + fc + "_channel_" + str(index),
                                      savefolder=weights_root)
                    weightmatrix_to_pgfplots(channel, title="weights_" + fc + "_channel_" + str(index),
                                      savefolder=weights_root)

        print("[VISUALISATION] weights visualised")
        print("[VISUALISATION] visualising training...")
        plot_training_curve(self.model_name)
        print("[VISUALISATION] visualised training")
        # print("[VISUALISATION] visualising faults...")
        # plot_fault_analysis(self.model_name)
        # print("[VISUALISATION] visualised faults")
        print("[VISUALISATION] visualising examples...")
        visualisations = {}
        examples_root = system_settings.get_visualisation_path(self.model_name) + config['examples_root'] + "/"
        for i in range(int(config['visualized_examples'])):
            net.forward()
            gt = net.blobs['label'].data  # ground truth
            ests = net.blobs['score'].data  # est
            if not os.path.exists(examples_root + str(i) + '/'):
                os.makedirs(examples_root + str(i) + '/')
            (fn, fp, tn, tp) = self.post_processor.get_note_statistics(gt, ests)
            plot_weightmatrix(np.transpose(self.post_processor.get_note_ests(ests)),
                              title="prediction_without_threshold", savefolder=examples_root + str(i) + '/')
            plot_weightmatrix(np.transpose(fn), title="false negatives", savefolder=examples_root + str(i) + '/')
            plot_weightmatrix(np.transpose(fp), title="false positives", savefolder=examples_root + str(i) + '/')
            plot_weightmatrix(np.transpose(tn), title="true negatives", savefolder=examples_root + str(i) + '/')
            plot_weightmatrix(np.transpose(tp), title="true positives", savefolder=examples_root + str(i) + '/')
            plot_weightmatrix(np.transpose(tp + fn), title="label_notes", savefolder=examples_root + str(i) + '/')
            plot_weightmatrix(np.transpose(tp + fp), title="prediction", savefolder=examples_root + str(i) + '/')
            plot_weightmatrix_color(
                np.transpose((10 * tp.astype(int)) - (5 * fn.astype(int)) - (10 * fp.astype(int))),
                title="prediction_colored", savefolder=examples_root + str(i) + '/')
            colored =(10 * tp.astype(int)) - (5 * fn.astype(int)) - (10 * fp.astype(int))
            with open(examples_root + str(i) + '/prediction_colored.dat','w+') as datafile:
                for x in range(colored.shape[0]):
                    for y in range(colored.shape[1]):
                        datafile.write(str(x) + ' ' + str(y) + ' ' + str(colored[x,y]) + '\n')
                    datafile.write('\n')
            spec_dim = 6
            data = np.array(np.squeeze(net.blobs['data'].data), copy=True)
            print data.shape
            for k in [(fn, -0.5), (tp, 1), (fp, -1)]:
                rows, columns = np.nonzero(np.transpose(k[0]))
                for index in range(rows.shape[0]):
                    freq = rows[index] * spec_dim + (spec_dim * 2 - 1)
                    lower = data[columns[index], freq - (spec_dim / 2):freq + (spec_dim / 2)] < np.mean(data)
                    higher = data[columns[index], freq - (spec_dim / 2):freq + (spec_dim / 2)] >= np.mean(data)
                    first = data[columns[index], freq - (spec_dim / 2):freq + (spec_dim / 2)]
                    second = data[columns[index], freq - (spec_dim / 2):freq + (spec_dim / 2)] * k[1]
                    data[columns[index], freq - (spec_dim / 2):freq + (spec_dim / 2)] = np.select([lower, higher],
                                                                                                  [first, second])
            plot_weightmatrix_color(
                np.transpose(data),
                title="data_colored", savefolder=examples_root + str(i) + '/')

            for blob in net.blobs:
                data_blob = net.blobs[blob].data
                if not (blob == "loss"):
                    visualisations[blob] = data_blob
            for blob in net.blobs:
                if not (blob == "loss"):
                    data = np.transpose(np.squeeze(visualisations[blob]))
                    if len(data.shape) == 2:
                        if not os.path.exists(examples_root + str(i) + '/'):
                            os.makedirs(examples_root + str(i) + '/')
                        if str(blob) == 'data':
                            plot_weightmatrix(data, title="original data", savefolder=examples_root + str(i) + '/',xlabel='framenummer',ylabel='MIDI nootnummer')
                        else:
                            plot_weightmatrix(data, title="data_" + str(blob), savefolder=examples_root + str(i) + '/')
                    elif len(data.shape) == 3:
                        for index in range(data.shape[1]):
                            if not os.path.exists(examples_root + str(i) + '/'):
                                os.makedirs(examples_root + str(i) + '/')
                            plot_weightmatrix(data[:, index, :], title="data_" + blob + "_channel_" + str(index),
                                              savefolder=examples_root + str(i) + '/')
            visualisations = {}
        print("[VISUALISATION] visualised examples")

    def make_precision_recall(self, teststats):
        for i in [0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9,0.95]:
            self.post_processor.set_threshold(i)
            self.test(teststats)
