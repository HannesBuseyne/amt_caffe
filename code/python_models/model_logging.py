""""
Created : December 26, 2016

Author: Hannes Buseyne
"""

import copy

import numpy as np


def check_accuracy(model_name, net, num_batches, iteration, postprocessor, statistics, train_loss):
    print "[VALIDATION] validating using threshold " + str(postprocessor.get_threshold()) + " ..."
    (true_positives, false_negatives, false_positives) = (0, 0, 0)
    (precision, recall, precision_recall) = (0, 0, 0)
    loss = 0
    for t in range(num_batches):
        fw = net.forward()
        loss += fw["loss"]
        gt = net.blobs['label'].data  # ground truth
        ests = net.blobs['score'].data  # est
        (fn, fp, _, tp) = postprocessor.get_note_statistics(gt, ests)
        # precision_recall per frame for f1-measure
        (precision, recall, precision_recall) = [sum(x) for x in zip((precision, recall, precision_recall),
                                                                     calculate_precision_recall(tp, fp, fn))]
        (true_positives, false_negatives, false_positives) = [sum(x) for x in
                                                              zip((true_positives, false_negatives, false_positives),
                                                                  (np.sum(tp), np.sum(fn), np.sum(fp)))]
    loss = float(loss) / num_batches
    if true_positives > 0:
        print "[VALIDATION] current accuracy : " + str(float(true_positives) / (
            true_positives + false_positives + false_negatives))
    print "[VALIDATION] current test loss : " + str(loss)
    statistics.add_iteration(model_name,
                             iteration, true_positives=true_positives, false_positives=false_positives,
                             false_negatives=false_negatives,
                             precision_recall=precision_recall, precision=precision, recall=recall,
                             test_loss=loss,
                             train_loss=copy.deepcopy(train_loss)
                             )


def check_faults(model_name, net, num_batches, postprocessor, statistics):
    print "[TESTING] testing using threshold " + str(postprocessor.get_threshold()) + " ..."
    (true_positives, false_negatives, false_positives) = (0, 0, 0)
    (precision, recall, precision_recall) = (0, 0, 0)
    for t in range(num_batches):
        net.forward()
        gt = net.blobs['label'].data  # ground truth
        ests = net.blobs['score'].data  # estimation
        (fn, fp, tn, tp) = postprocessor.get_note_statistics(gt, ests)
        (true_positives, false_negatives, false_positives) = [sum(x) for x in
                                                              zip((
                                                                  true_positives,
                                                                  false_negatives,
                                                                  false_positives),
                                                                  (np.sum(tp),
                                                                   np.sum(fn),
                                                                   np.sum(fp)))]
        statistics.add_fault_position_metrics(model_name, *postprocessor.get_fault_note_metrics(fn, fp, tp))
        statistics.add_faults_per_note(model_name, tp.sum(axis=1), fp.sum(axis=1), fn.sum(axis=1), tn.sum(axis=1))
        (precision, recall, precision_recall) = [sum(x) for x in zip((precision, recall, precision_recall),
                                                                     calculate_precision_recall(tp, fp, fn))]
        statistics.add_fault_combination_matrix(model_name, *get_fault_combinations(tp, fp, fn, tn))
        statistics.add_faults_per_polyphony(model_name, *get_faults_per_polyphony(tp, fp, fn))
    if true_positives > 0:
        print "[TESTING] test accuracy : " + str(float(true_positives) / (
            true_positives + false_positives + false_negatives))
    statistics.add_test(model_name,
                        true_positives=true_positives, false_positives=false_positives, false_negatives=false_negatives,
                        precision_recall=precision_recall, precision=precision, recall=recall,
                        threshold=postprocessor.get_threshold()
                        )



def check_faults_multiple_models(model_name, nets, num_batches, postprocessors, weights,combined_threshold,
                                 statistics):
    print "[TESTING] testing..."
    (true_positives, false_negatives, false_positives) = (0, 0, 0)
    (precision, recall, precision_recall) = (0, 0, 0)
    for t in range(num_batches):
        for net in nets:
            net.forward()
        gt = nets[0].blobs['label'].data  # ground truth
        predictions = list(postprocessor.get_predictions(net.blobs['score'].data) for (postprocessor, net) in
                           zip(postprocessors, nets))  # est
        prediction = np.zeros(shape=predictions[0].shape)
        for index, pred in enumerate(predictions):
            prediction += (weights[index] * pred)
        prediction = np.greater(prediction, np.full(prediction.shape, combined_threshold))
        (fn, fp, tn, tp) \
            = postprocessors[0].check_predictions(gt, prediction)
        (true_positives, false_negatives, false_positives) = [sum(x) for x in
                                                              zip((
                                                                  true_positives,
                                                                  false_negatives,
                                                                  false_positives),
                                                                  (np.sum(tp),
                                                                   np.sum(fn),
                                                                   np.sum(fp)))]
        statistics.add_fault_position_metrics(model_name,
                                              *postprocessors[0].get_fault_note_metrics(
                                                  fn, fp, tp))
        statistics.add_faults_per_note(model_name, tp.sum(axis=1),
                                       fp.sum(axis=1), fn.sum(axis=1),
                                       tn.sum(axis=1))
        (precision, recall, precision_recall) = [sum(x) for x in zip(
            (precision, recall, precision_recall),
            calculate_precision_recall(tp, fp, fn))]
        statistics.add_fault_combination_matrix(model_name, *get_fault_combinations(tp, fp, fn, tn))
        statistics.add_faults_per_polyphony(model_name, *get_faults_per_polyphony(tp, fp, fn))
    if true_positives > 0:
        print "[TESTING] test accuracy : " + str(float(true_positives) / (
            true_positives + false_positives + false_negatives))
    statistics.add_test(model_name,
                        true_positives=true_positives, false_positives=false_positives, false_negatives=false_negatives,
                        precision_recall=precision_recall, precision=precision, recall=recall,
                        threshold=postprocessors[-1].get_threshold()
                        )


def calculate_precision_recall(true_positives, false_positives, false_negatives):
    if (np.sum(true_positives) > 0):
        precision = float(np.sum(true_positives)) / (np.sum(true_positives) + np.sum(false_positives))
        recall = float(np.sum(true_positives)) / (np.sum(true_positives) + np.sum(false_negatives))
        precision_recall = 2 * float(np.sum(true_positives)) / (np.sum(
            true_positives) + np.sum(false_positives)) * float(np.sum(true_positives)) / (np.sum(
            true_positives) + np.sum(false_negatives))
        return (precision,recall,precision_recall)
    else:
        return (0,0,0)


def get_faults_per_polyphony(tp, fp, fn):
    pos_per_note = (tp + fn).sum(axis=1)
    fp_per_polyphony = np.zeros(shape=(50))
    fn_per_polyphony = np.zeros(shape=(50))
    examples = np.zeros(shape=(50))
    for i in range(pos_per_note.shape[0]):
        fp_per_polyphony[pos_per_note[i]] += np.sum(fp[i, :])
        fn_per_polyphony[pos_per_note[i]] += np.sum(fn[i, :])
        examples[pos_per_note[i]] += 1
    return (fp_per_polyphony, fn_per_polyphony,examples)


def get_fault_combinations(true_positives, false_positives, false_negatives, true_negatives):
    positives = true_positives + false_negatives
    negatives = true_negatives + false_positives
    fp_combinations = np.zeros(shape=(false_positives.shape[1], false_positives.shape[1]))
    fn_combinations = np.zeros(shape=(false_positives.shape[1], false_positives.shape[1]))
    pos_combinations = np.zeros(shape=(false_positives.shape[1], false_positives.shape[1]))
    neg_combinations = np.zeros(shape=(false_positives.shape[1], false_positives.shape[1]))
    for comb_array, faults in zip((fp_combinations, fn_combinations, neg_combinations, pos_combinations),
                                  (false_positives, false_negatives, negatives, positives)):
        for i in range(faults.shape[0]):
            #on the indices where a something in the first is present : save add all other present notes in the frame to the combination matrix on the index of the present note.
            pos_indices = np.where(positives[i, :] > 0)
            for index in pos_indices[0]:
                comb_array[index, :] += faults[i, :].astype(int)
    return (fp_combinations, fn_combinations, pos_combinations, neg_combinations)



def add_dimenstions(true_positives, true_negatives, false_positives, false_negatives):
    return (np.expand_dims(true_positives, axis=0),
            np.expand_dims(true_negatives, axis=0),
            np.expand_dims(false_positives, axis=0),
            np.expand_dims(false_negatives, axis=0))
