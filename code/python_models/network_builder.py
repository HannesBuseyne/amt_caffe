""""
Created : December 25, 2016

Author: Hannes Buseyne
"""

from caffe import layers as L, params as P


# helper function for common structures
def conv_relu(bottom, ks, nout, stride=1, pad=0, group=1):
    conv = L.Convolution(bottom, kernel_w=ks, kernel_h=1, stride=stride,
                         num_output=nout, pad_w=pad, group=group, weight_filler=dict(type='xavier'))
    return conv, L.ReLU(conv, in_place=True, negative_slope=0.01)


# another helper function
def fc_relu(bottom, nout):
    fc = L.InnerProduct(bottom, num_output=nout)
    return fc, L.ReLU(fc, in_place=True)


# yet another helper function
def max_pool(bottom, ks, stride=1):
    return L.Pooling(bottom, pool=P.Pooling.MAX, kernel_w=ks, kernel_h=1, stride=stride)
