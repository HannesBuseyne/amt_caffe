import caffe
from caffe import layers as L

from model import Model
from network_builder import conv_relu, max_pool
import configs


class OneLayerModel(Model):
    def __init__(self, solver, data_source, data_part, data_preparation, post_processor,model_conf):
        if "conv_length" in model_conf:
            self.conv_length = configs.parse_int_config(model_conf,'conv_length')
        else:
            self.conv_length = 100
        if "pool" in model_conf:
            self.pool = configs.parse_int_config(model_conf,"pool")
        else:
            self.pool = 3
        Model.__init__(self, "one_layer_model", solver, data_source=data_source, data_part=data_part,
                       data_preparation=data_preparation, post_processor=post_processor,preprocessor=model_conf["preprocessor"])

    def build_net(self, data_layer_params):
        # setup the python data layer
        n = caffe.NetSpec()
        n.data, n.label = L.Python(module='MultipleLabelLayer', layer="MultipleLabelLayer",ntop =2, param_str=str(data_layer_params))

        # the net itself
        n.conv1, n.relu1 = conv_relu(n.data, self.conv_length, 5)
        n.score = L.InnerProduct(n.conv1, num_output=self.get_number_of_outputs())
        n.loss = L.EuclideanLoss(n.score, n.label)

        return str(n.to_proto())
