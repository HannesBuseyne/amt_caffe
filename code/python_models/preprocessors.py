'''
Created on Apr 1, 2016

@author: Daan Seynaeve
'''

import numpy as np


class Preprocessor:
    def __init__(self, scale=(0, 1), keep_dynamics=False):
        self.scale = scale
        self.keep_dynamics = keep_dynamics

    def do_labels(self):
        return False

    def apply(self, data, verbose=True):
        '''
        normalises the data
        '''
        if (verbose): print("[PREPROC] USING DAANS PREPROCESSOR")
        if (verbose): print("[PREPROC] before:")
        if (verbose): self.report_stats(data)
        if (verbose): print("[PREPROC] after:")

        # avoid division by zero
        data += 1

        # remove note dynamics-
        if not self.keep_dynamics:
            for i in range(np.shape(data)[0]):
                data[i, :] = data[i, :] / np.sum(data[i, :])
        self.report_stats(data)
        # scale to correct range
        data -= np.min(data)
        data /= np.max(data)
        data *= (self.scale[1] - self.scale[0])
        data += self.scale[0]

        if (verbose): self.report_stats(data)

        return data

    def apply_to_frame(self, frame, min, max):
        frame += 1
        frame = frame / np.sum(frame)
        frame -= min
        frame /= max
        frame *= (self.scale[1] - self.scale[0])
        frame += self.scale[0]
        return frame

    def report_stats(self, data):
        print("mean: %s, std: %s" % (np.mean(data), np.std(data)))
        print("min: %s, max: %s" % (np.min(data), np.max(data)))

class NormalizingPreprocessor:
    def __init__(self):
        pass
    def do_labels(self):
        return False
    def apply(self,data,verbose=True):
        if (verbose): print("[PREPROC] USING SIGTIA PREPROCESSOR")
        if (verbose): print("[PREPROC] before:")
        if (verbose): self.report_stats(data)
        if (verbose): print("[PREPROC] after:")
        data -= np.mean(data)
        data  /= np.std(data)
        return data

    def report_stats(self, data):
        print("mean: %s, std: %s" % (np.mean(data), np.std(data)))
        print("min: %s, max: %s" % (np.min(data), np.max(data)))

class FramePreprocessor:
    def __init__(self, scale=(0, 1)):
        self.scale = scale

    def apply(self, data, verbose=True):
        '''
        normalises the data
        '''
        if (verbose): print("[PREPROC] USING FRAME SCALER PREPROCESSOR")
        if (verbose): print("[PREPROC] before:")
        if (verbose): self.report_stats(data)
        if (verbose): print("[PREPROC] after:")

        # remove note dynamics-
        for i in range(np.shape(data)[0]):
            data[i, :] -= np.min(data[i, :])
            if np.max(data[i, :]) > 0:
                data[i, :] /= np.max(data[i, :])
                data[i, :] *= (self.scale[1] - self.scale[0])
            data[i, :] += self.scale[0]

        if (verbose): self.report_stats(data)

        return data

    def do_labels(self):
        return True

    def report_stats(self, data):
        print("mean: %s, std: %s" % (np.mean(data), np.std(data)))
        print("min: %s, max: %s" % (np.min(data), np.max(data)))

class FrameNormalizer:
    def __init__(self):
        pass

    def apply(self, data, verbose=True):
        '''
        normalises the data
        '''
        if (verbose): print("[PREPROC] USING FRAME NORMALIZING PREPROCESSOR")
        if (verbose): print("[PREPROC] before:")
        if (verbose): self.report_stats(data)
        if (verbose): print("[PREPROC] after:")

        # remove note dynamics-
        for i in range(np.shape(data)[0]):
            if np.max(data[i, :]) > 0:
                data[i, :] -= np.mean(data[i,:])
                data[i, :] /= np.std(data[i,:])

        if (verbose): self.report_stats(data)

        return data

    def do_labels(self):
        return True

    def report_stats(self, data):
        print("mean: %s, std: %s" % (np.mean(data), np.std(data)))
        print("min: %s, max: %s" % (np.min(data), np.max(data)))


def get_preprocessor(prep_str):
    if prep_str == "daan_preprocessor":
        return Preprocessor()
    elif prep_str == "normalizer":
        return NormalizingPreprocessor()
    elif prep_str == "framepreprocessor":
        return FramePreprocessor()
    else:
        return FrameNormalizer()