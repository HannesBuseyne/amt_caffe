""""
Created : December 29, 2016

Author: Hannes Buseyne
"""

import caffe
from caffe import layers as L

from model import Model
from network_builder import conv_relu, max_pool


class SevenLayerModel(Model):
    def __init__(self, solver, data_source, data_part, data_preparation, post_processor,
                 model_conf):
        Model.__init__(self, "slm", solver, data_source=data_source, data_part=data_part,
                       data_preparation=data_preparation, post_processor=post_processor,preprocessor=model_conf["preprocessor"])
        if "conv_length" in model_conf:
            self.conv_length = configs.parse_int_config(model_conf,'conv_length')
        else:
            self.conv_length = 50
        if "pool" in model_conf:
            self.pool = configs.parse_int_config(model_conf,"pool")
        else:
            self.pool = 3

    def build_net(self, data_layer_params):
        # setup the python data layer
        n = caffe.NetSpec()
        n.data, n.label = L.Python(module='MultipleLabelLayer', layer="MultipleLabelLayer",
                                   ntop=2, param_str=str(data_layer_params))

        # the net itself
        n.conv1, n.relu1 = conv_relu(n.data, self.conv_length, 8, pad=self.conv_length / 2)
        n.conv2, n.relu2 = conv_relu(n.relu1, self.conv_length, 15, pad=self.conv_length / 2)
        n.pool2 = max_pool(n.relu2, 3, stride=3)
        n.conv3, n.relu3 = conv_relu(n.pool2, self.conv_length, 30, pad=self.conv_length / 2)
        n.conv4, n.relu4 = conv_relu(n.relu3, self.conv_length, 45, pad=self.conv_length / 2)
        n.pool5 = max_pool(n.relu4, 3, stride=3)
        n.conv6, n.relu6 = conv_relu(n.pool5, self.conv_length, 60, pad=self.conv_length / 2)
        n.conv7, n.relu7 = conv_relu(n.relu6, self.conv_length, 70, pad=self.conv_length / 2)
        n.pool8 = max_pool(n.relu7, 3, stride=3)
        n.conv9, n.relu9 = conv_relu(n.pool8, self.conv_length, 88, pad=self.conv_length / 2)
        n.score = L.InnerProduct(n.relu9, num_output=self.get_number_of_outputs())
        n.loss = L.EuclideanLoss(n.score, n.label)

        return str(n.to_proto())
