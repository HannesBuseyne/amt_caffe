import os

from configobj import ConfigObj


def get_defaultconf():
    """
    get the default configuration for the network
    :return: the default configuration for the network
    """
    conf = ConfigObj()
    conf['mode'] = ['train', 'test', 'visualize']
    conf['labels'] = 'notes',
    conf['data_preparation'] = {'transform': 'cqt',
                                'slice': 2 ** 16,
                                # not implemented yet
                                'use_gradient': False,
                                'frame_length': 2048,
                                'cutoff': None,
                                'spec_dim': 6,
                                'filter_useful': False}
    conf['data_loader'] = {
        'MAPS_excerpt': None,
        'MusicNet_excerpt': None,
        'random_minute': True
    }
    conf['post_processing'] = {'moving_threshold': False,
                               'moving_method': 'lin',
                               'threshold': 0.35,
                               'min_threshold': 0.15,
                               'lin_step': 0.01,
                               'exp_factor': 0.9,
                               'attention_aggregate': 'mean'}
    conf['solver'] = {'learning_rate': 0.001,
                      # batch size for validation
                      'batch_size': 256,
                      # momentum for training
                      'momentum': 0.9,
                      # step size for learning rate
                      'step': 15000,
                      #
                      'gamma': 0.1,
                      'validation_step': 1000,
                      'max_iter': 25000,
                      'decay_factor': 0.0,
                      'decay_method': "L2",
                      'snapshot': 5000,
                      'test_iterations': 12,
                      'step_size': 1000}
    conf['model'] = {  # type can either be normal or combined
        'type': 'normal',
        # the number of convolutional layers, this can be 2, 5 or 7
        'layers': 2,
        # name that will be given to the model
        'name': 'daan_model'}
    conf['test'] = 'test'
    conf['visualization'] = {'precision_recall': 'precision_recall',
                             'examples_root': 'examples',
                             'visualized_examples': 10}
    return conf

def get_combinedconf():
    """
    get the default configuration for the combined networks
    :return: the default configuration for the network
    """
    conf = ConfigObj()
    conf['mode'] = ['train', 'test', 'visualize']
    conf['model_name'] = 'default'
    conf['models'] ={}
    for model in ['model1','model2']:
        conf['models'][model] = {'weight':1}
        conf['models'][model]['labels'] = 'notes'
        conf['models'][model]['data_loader'] = {
            'MAPS_excerpt': 59,
            'MusicNet_excerpt': 150,
            'random_minute': False
        }
        conf['models'][model]['post_processing'] = {'moving_threshold': False,
                                                    'moving_method': 'lin',
                                                    'threshold': 0.35,
                                                    'min_threshold': 0.15,
                                                    'lin_step': 0.01,
                                                    'exp_factor': 0.9,
                                                    'attention_aggregate': 'mean'}
        if model == 'model1' :
            conf['models'][model]['data_preparation'] = {'transform': 'cqt',
                                        'slice': 2 ** 12,
                                        # not implemented yet
                                        'use_gradient': False,
                                        'frame_length': 2048,
                                        'cutoff': None,
                                        'spec_dim': 6,
                                        'filter_useful': False}
        else:
            conf['models'][model]['data_preparation'] = {'transform': 'cqt',
                                        'slice': 2 ** 16,
                                        # not implemented yet
                                        'use_gradient': False,
                                        'frame_length': 2048,
                                        'cutoff': None,
                                        'spec_dim': 6,
                                        'filter_useful': False}
        conf['models'][model]['model'] = {  # type can either be normal or combined
            'type': 'combined',
            # the number of convolutional layers, this can be 2, 5 or 7
            'layers': 2,
            # name that will be given to the model
            'name': 'daan_model'}
        conf['model'] = {'type':'combined'}
        conf['models'][model]['solver'] = {'learning_rate': 0.001,
                          # batch size for validation
                          'batch_size': 256,
                          # momentum for training
                          'momentum': 0.9,
                          # step size for learning rate
                          'step': 15000,
                          #
                          'gamma': 0.1,
                          'validation_step': 1000,
                          'max_iter': 25000,
                          'decay_factor': 0.0,
                          'decay_method': "L2",
                          'snapshot': 5000,
                          'test_iterations': 12,
                          'step_size': 1000}
    conf['test'] = 'test'
    conf['visualization'] = {'precision_recall': 'precision_recall',
                             'examples_root': 'examples',
                             'visualized_examples': 10}
    conf['combined_threshold'] = 1.5
    return conf


def write_conf(conf, conf_name):
    """
    write the configuration to a file
    :param conf: a configuration as ConfigObj
    :param conf_name: the name of the file, the file will be located in the AMT root/config directory
    """
    # path = system_settings.get_AMT_root()+ 'config/'
    path = '/mnt/DATA/Thesis/AMT_caffe/config/'
    if not os.path.exists(path):
        os.makedirs(path)
    with open(path + conf_name, 'w') as configfile:
        conf.write(configfile)


def parse_int_config(config, name):
    if config[name] == 'None':
        return None
    else:
        return int(config[name])

def parse_bool_config(config,name):
    if config[name] == 'True':
        return True
    else:
        return False

# write_conf(get_combinedconf(),'defaultcombined.conf')
