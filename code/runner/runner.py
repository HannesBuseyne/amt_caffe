import system_settings
system_settings.prepare_path()
from solver import Solver
from one_layer_model import OneLayerModel
from daan_model import DaanModel
from five_layer_model import FiveLayerModel
from seven_layer_model import SevenLayerModel
from data_preparation import DataPreparation
from test_statistics import TestStatistics
from label_generation import NoteLabelgeneration, AttentionLabelGeneration, AutoEncoderLabelGeneration
from postprocessors import NormalPostprocessor, AttentionPostprocessor, AutoEncoderPostprocessor
from combined_model import CombinedModel
from autoencoder import AutoEncoderModel
import argparse
import sys
from configobj import ConfigObj

#####################################################################################################
#Helper functions
######################################################################################################
def parse_label(config):
    if config['labels'] == 'notes':
        label_generation = NoteLabelgeneration()
        post_processor = NormalPostprocessor(config['post_processing'])
    elif config['labels'] == 'attention':
        label_generation = AttentionLabelGeneration(int(config['data_preparation']['spec_dim']))
        post_processor = AttentionPostprocessor(config['post_processing'])
    else:
        raise Exception("label not supported")
    return (label_generation,post_processor)

def parse_model(config,solver,dataset,part,post_processor,data_preparation):
    if int(config['model']['layers']) == 1:
        model = OneLayerModel(solver, data_source=dataset, data_part=part, data_preparation=data_preparation,
                          post_processor=post_processor,model_conf=config['model'])
    elif int(config['model']['layers']) == 2:
        model = DaanModel(solver, data_source=dataset, data_part=part, data_preparation=data_preparation,
                          post_processor=post_processor,model_conf=config['model'])
    elif int(config['model']['layers']) == 5:
        model = FiveLayerModel(solver, data_source=args.dataset, data_part=args.part, data_preparation=data_preparation,
                               post_processor=post_processor,model_conf=config['model'])
    elif int(config['model']['layers']) == 7:
        model = SevenLayerModel(solver, data_source=args.dataset, data_part=args.part,
                                data_preparation=data_preparation, post_processor=post_processor,model_conf=config['model'])
    else:
        print "only 2, 5 or 7 layers are supported "
        sys.exit()
    return model


# configs.write_conf(configs.get_combinedconf(),'combinedconfig.conf')

parser = argparse.ArgumentParser(
    description='Train, test or visualize a model with specified data and given options file.')
parser.add_argument('dataset', metavar='Data', type=str,
                    help='a dataset to train on')
parser.add_argument('part', metavar='Part', type=str,
                    help='the part of the given dataset')
parser.add_argument('config', metavar='config', type=str,
                    help='configfile for the algorithm')
args = parser.parse_args()

#######################################################################################################
# argument parsing
######################################################################################################
if args.dataset != 'MAPS' and args.dataset != 'MusicNet':
    print "only MAPS and MusicNet dataset are supported"
    sys.exit()
elif args.dataset == 'MAPS':
    if not (args.part in ['MUS','UCHO', 'ISOL/NO','MUS_1', 'MUS_2', 'MUS_3', 'MUS_4']):
        print "Only 'MUS','UCHO','ISOL/NO','MUS_1','MUS_2','MUS_3' and'MUS_4' are supported for MAPS"
        sys.exit()
else:
    if not (args.part in ['MUS', 'Solo Piano', 'SP_1','SP_2','SP_3','SP_4','Piano Quintet', 'Piano Trio', 'String Quartet', 'Clarinet Quintet',
                          'Pairs Clarinet-Horn-Bassoon', 'Wind Quintet', 'Accompanied Cello', 'Accompanied Clarinet',
                          'String Sextet', 'Horn Piano Trio', 'Solo Cello', 'Accompanied Violin']):
        print "supported values for MusicNet are 'MUS','Solo Piano','Piano Quintet','Piano Trio','String Quartet','Clarinet Quintet', 'Pairs Clarinet-Horn-Bassoon','Wind Quintet','Accompanied Cello','Accompanied Clarinet','String Sextet','Horn Piano Trio','Solo Cello' and'Accompanied Violin'"
        sys.exit()
config = None
try:
    config = ConfigObj(args.config)
except:
    print "configuration file was not valid"

#######################################################################################################
# Building Solver and model
######################################################################################################
model, label_generation, post_processor = (None, None, None)
if config['model']['type'] == 'normal':
    solver = Solver(config['solver'])
    (label_generation, post_processor) = parse_label(config)
    data_preparation = DataPreparation(config['data_preparation'], config['data_loader'], label_generation)
    model = parse_model(config,solver,args.dataset,args.part, post_processor,data_preparation)
elif config['model']['type'] == 'combined':
    models = []
    weights = []
    for model in config['models']:
        solver = Solver(config['models'][model]['solver'])
        (label_generation, post_processor) = parse_label(config['models'][model])
        data_preparation = DataPreparation(config['models'][model]['data_preparation'], config['models'][model]['data_loader'], label_generation)
        models.append(parse_model(config['models'][model], solver, args.dataset, args.part, post_processor, data_preparation))
        weights.append(float(config['models'][model]['weight']))
    model = CombinedModel(config['model_name'] + '_' + args.dataset + '_' + args.part,models,weights,float(config['combined_threshold']))
elif config['model']['type'] == 'autoencoder':
    solver = Solver(config['solver'])
    (label_generation, post_processor) = (
    AutoEncoderLabelGeneration(int(config['data_preparation']['spec_dim'])), AutoEncoderPostprocessor())
    data_preparation = DataPreparation(config['data_preparation'], config['data_loader'], label_generation)
    model = AutoEncoderModel(solver, data_source=args.dataset, data_part=args.part,
                             data_preparation=data_preparation, post_processor=post_processor)
else:
    print "invalid mode"
    sys.exit()

#####################################################################################################
# Training testing and visualizing
#####################################################################################################
for phase in config['mode']:
    stats = TestStatistics(config['test'])
    if phase == 'train':
        model.train(stats)
    elif phase == 'test':
        model.test(stats)
    elif phase == 'visualize':
        model.visualize(config['visualization'])
    elif phase == 'make_precision_recall':
        model.make_precision_recall(stats)
    else:
        print "invalid phase, only 'train','test','visualize' and 'make_precision_recall' are supported "
        sys.exit()
