"""
Created : December 24, 2016

Author: Hannes Buseyne
"""

import sys

AMT_root = '/workspace/AMT_caffe/'
MAPS_root = '/workspace/MAPS data/'
MusicNet_root = '/workspace/MusicNet data/'
nsgt_root = '/workspace/automaticmusictranscription/python/nsgt-master'
gpu = False

def get_AMT_root():
    """
    the root of this git
    :return: the root of this git
    """
    return AMT_root

def get_MAPS_root():
    """
    the root of the uncompressed MAPS data
    :return: the root of the uncompressed MAPS data
    """
    return MAPS_root


def get_MusicNet_root():
    """
    returns the root of the compressend musicNet numpy array and metadata
    :return: the root of the compressend musicNet numpy array and metadata
    """
    return MusicNet_root

def use_gpu():
    """
    should we use the gpu to train?
    :return: True if the algorithm can use the GPU
    """
    return gpu

def prepare_path():
    """
    prepares the system python path and specifies important imports
    """
    sys.path.extend([nsgt_root,
                     AMT_root +'code/runner/',
                     AMT_root + "code/data_preparation/",
                     AMT_root + "code/python_models/",
                     AMT_root + "code/solvers/",
                     AMT_root + "code/visualisation/",
                     "/home/hannes/Programs/Tools/caffe_library/python/",
                     "/home/hannes/anaconda2/lib/"])
    import matplotlib as mpl
    mpl.use('Agg')

def get_net_path(model_name):
    """
    returns the path of the model definition, given the name of the model
    :param model_name: the name of the model
    :return: the path of the model definition
    """
    return AMT_root + "models/"+model_name+"/definition/"

def get_snapshot_path(model_name):
    """
    returns the path for the snapshots, given the name of the model
    :param model_name: the name of the model
    :return: the path to save the snapshots of the model weights to.
    """
    return AMT_root + "models/"+model_name+"/trained/"+model_name

def get_net_stats_path(model_name):
    """
    returns the path of the network statistics given the model name
    :param model_name: name of the model
    :return: the path of the network statistics
    """
    return AMT_root + "models/" + model_name + "/"


def get_visualisation_path(model_name):
    """
    returns the path of the visualisations, given the model name
    :param model_name: name of the model
    :return: the path of the visualisations
    """
    return AMT_root + "visualisations/" + model_name + "/"


def get_data_path(source, part, data_preparation):
    """
    returns the path to the data, given the source dataset, the part of the source dataset and the data preparation
    :param source: the source dataset
    :param part: the part of the source dataset
    :param data_preparation: a data preparation object describing the way data is prepared.
    :return: the data path where prepared data is saved
    """
    return AMT_root + "data/" + source + "/" + str(part) + "/" + data_preparation.get_path()
