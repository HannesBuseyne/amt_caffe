import system_settings
system_settings.prepare_path()
from log_plotter import plot_precision_recall
import argparse
from configobj import ConfigObj

parser = argparse.ArgumentParser(
    description='make a precision recall diagram from acquired test files.')
parser.add_argument('config', metavar='config', type=str,
                    help='configfile for the visualizer')
args = parser.parse_args()


#######################################################################################################
# argument parsing
######################################################################################################
config = None
try:
    config = ConfigObj(args.config)
except:
    print "configuration file was not valid"

print args.config
plot_precision_recall(config['legend'],config['filename'],title = config['title'])