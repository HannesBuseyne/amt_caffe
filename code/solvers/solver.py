""""
Created : December 25, 2016

Author: Hannes Buseyne
"""
import os

import caffe
import system_settings


class Solver:
    def __init__(self, solver_config):
        self.lr = float(solver_config['learning_rate'])
        self.batch_size = int(solver_config['batch_size'])
        self.momentum = float(solver_config['momentum'])
        self.step = int(solver_config['step'])
        self.gamma = float(solver_config['gamma'])
        self.validation_step = int(solver_config['validation_step'])
        self.max_iter = int(solver_config['max_iter'])
        self.weight_decay = float(solver_config['decay_factor'])
        self.norm = solver_config['decay_method']
        self.snapshot = int(solver_config['snapshot'])
        self.test_iterations = int(solver_config['test_iterations'])
        self.step_size = int(solver_config['step_size'])

    def get_test_iterations(self):
        return self.test_iterations

    def get_step_size(self):
        return self.validation_step

    def get_batch_size(self):
        return self.batch_size

    def get_max_iteration(self):
        return self.max_iter

    def get_solver(self):
        self.make_solver()
        return caffe.SGDSolver(system_settings.get_net_path(self.model_name)+"solver.prototxt")

    def get_number_of_steps(self):
        return self.max_iter/self.validation_step

    def set_model_name(self,model):
        self.model_name = model

    def make_solver(self):
        if self.model_name is None:
            raise Exception("No model name specified")
        if not os.path.exists(system_settings.get_net_path(self.model_name)):
            os.makedirs(system_settings.get_net_path(self.model_name))
        if not os.path.exists(system_settings.get_snapshot_path(self.model_name)):
            os.makedirs(system_settings.get_snapshot_path(self.model_name))
        with open(system_settings.get_net_path(self.model_name)+"solver.prototxt", 'w') as f:
            f.write(self.to_str())

    def to_str(self):
        if self.model_name is None:
            raise Exception("No model name specified")
        solver =  "train_net: \"" + system_settings.get_net_path(self.model_name) + "trainnet.prototxt\" \n" \
        + "test_net: \"" + system_settings.get_net_path(self.model_name) + "valnet.prototxt\" \n" \
        + "snapshot_prefix: \"" + system_settings.get_snapshot_path(self.model_name) +"\"\n" \
        + "snapshot : " + str(self.snapshot) +"\n"\
        + "test_iter: 0 \n" \
        + "test_interval: 1000000 \n" \
        + "test_compute_loss: false \n" \
        + "lr_policy: \"step\"\n"\
        + "display : 500 \n" \
        + "base_lr : " + str(self.lr) +"\n"\
        + "stepsize : " + str(self.step) +"\n"\
        + "gamma : " + str(self.gamma)+"\n"\
        + "momentum : " + str(self.momentum) +"\n"\
        + "type : \"nesterov\"\n"

        if not (self.weight_decay == None) :
            solver +="weight_decay : " + str(self.weight_decay) +"\n"
            solver += "regularization_type : \"" + str(self.norm) + "\"\n"
        if system_settings.use_gpu():
            solver += "solver_mode : GPU"
        else:
            solver += "solver_mode : CPU"

        return solver
