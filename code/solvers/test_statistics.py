""""
Created : December 25, 2016

Author: Hannes Buseyne
"""

import csv
import itertools

import numpy as np
import system_settings


class TestStatistics:
    def __init__(self, test_file):
        self.test_file = test_file
        self.models= {}
        self.tests={}

    def open_test_from_model(self, model_name):
        with open(system_settings.get_net_stats_path(model_name) + "test.csv", 'rb') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
            for index, row in enumerate(spamreader):
                if index > 0:
                    faults = [int(i) for i in row[1:]]
                    self.tests[model_name] = faults
        with open(system_settings.get_net_stats_path(model_name) + "faults_per_note.csv", 'rb') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
            for row in spamreader:
                faults = [int(i) for i in row[1:]]
                if row[0] == "FP":
                    self.false_positives[model_name] = faults
                if row[0] == "FN":
                    self.false_negatives[model_name] = faults
                if row[0] == "TP":
                    self.true_positives[model_name] = faults
                if row[0] == "TN":
                    self.true_negatives[model_name] = faults

    def add_model(self,model_name):
        self.models[model_name] = TrainingStats(model_name)

    def add_test(self, model_name, threshold, true_positives, false_positives, false_negatives, precision_recall,
                 precision, recall):
        stats = self.get_test_stats(model_name)
        test_f1 = 0
        if precision > 0 or recall > 0:
            test_f1 = float(precision_recall) / (precision + recall)
        if true_positives > 0:
            test_accuracy = float(true_positives) / (
                true_positives + false_positives + false_negatives)
            test_precision = float(true_positives) / (true_positives + false_positives)
            test_recall = float(true_positives) / (true_positives + false_negatives)
            stats.addTest(threshold, test_accuracy, test_precision, test_recall, test_f1)
        else:
            stats.addTest(threshold, 0, 0, 0, test_f1)

    def get_test_stats(self, model_name):
        if not (model_name in self.tests):
            self.tests[model_name] = TestStats(model_name, self.test_file)
        return self.tests[model_name]

    def add_iteration(self, model_name, iteration, train_loss, test_loss, true_positives, false_positives,
                      false_negatives, precision_recall, precision, recall):
        if not(model_name in self.models):
            self.models[model_name] = TrainingStats(model_name)
        test_f1 = 0
        if precision > 0 or recall > 0:
            test_f1 = float(precision_recall) / (precision + recall)
        if true_positives > 0:
            test_accuracy = float(true_positives) / (
                true_positives + false_positives + false_negatives)
            test_precision = float(true_positives) / (true_positives + false_positives)
            test_recall = float(true_positives) / (true_positives + false_negatives)
            self.models[model_name].add_iteration(iteration, train_loss, test_loss, test_accuracy, test_precision,
                                                  test_recall, test_f1)
        else:
            self.models[model_name].add_iteration(iteration, train_loss, test_loss, 0, 0, 0, test_f1)

    def add_fault_position_metrics(self, model_name, fn_before, fn_after, fp_before, fp_after, present_tp, present_fp,
                                   present_fn,
                                   tp_len, fp_len, fn_len):
        stats = self.get_test_stats(model_name)
        stats.add_fault_position_metrics(fn_before, fn_after, fp_before, fp_after, present_tp, present_fp, present_fn,
                                         tp_len, fp_len, fn_len)

    def add_faults_per_note(self, model_name, tp_per_note, fp_per_note, fn_per_note, tn_per_note):
        stats = self.get_test_stats(model_name)
        stats.add_faults_per_note(tp_per_note, fp_per_note, fn_per_note, tn_per_note)

    def add_fault_combination_matrix(self, model_name, fp_per_combination, fn_per_combination, pos_per_combination,
                                     neg_per_combination):
        stats = self.get_test_stats(model_name)
        stats.add_fault_combination_matrix(fp_per_combination, fn_per_combination, pos_per_combination,
                                           neg_per_combination)

    def add_faults_per_polyphony(self, model_name, fp_per_polyphony, fn_per_polyphony,examples):
        stats = self.get_test_stats(model_name)
        stats.add_faults_per_polyphony(fp_per_polyphony, fn_per_polyphony,examples)

    def write_to_csv(self):
        for model in self.models:
            self.models[model].write_to_csv()
        for test in self.tests:
            self.tests[test].write_to_csv()


class TrainingStats:
    def __init__(self,model_name):
        self.model_name=model_name
        self.iterations=[]

    def add_iteration(self,iteration,train_loss,test_loss,test_accuracy,test_precision,test_recall,test_f1):
        self.iterations.append([iteration,train_loss,test_loss,test_accuracy,test_precision,test_recall,test_f1])

    def write_to_csv(self):
        with open(system_settings.get_net_stats_path(self.model_name)+"training.csv", 'wb') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter=',',
                                    quotechar='|', quoting=csv.QUOTE_MINIMAL)
            spamwriter.writerow(
                ["iteraton ", "train loss", "test loss", "test accuracy", "test precision", "test recall", "test F1"])
            for row in self.iterations:
                spamwriter.writerow(row)


class TestStats:
    def __init__(self, model_name, test_file):
        self.test_file = test_file
        self.model_name = model_name
        self.tests = {}
        self.fn_before=np.zeros(shape=88)
        self.fn_after = np.zeros(shape=88)
        self.fp_before = np.zeros(shape=88)
        self.fp_after = np.zeros(shape=88)
        self.present_fp = np.zeros(shape=88)
        self.present_fn = np.zeros(shape=88)
        self.present_tp = np.zeros(shape=88)
        self.fault_metrics = np.zeros(shape=(3))
        (self.tp_per_note, self.fp_per_note, self.fn_per_note, self.tn_per_note) = (None, None, None, None)
        (self.fp_per_polyphony, self.fn_per_polyphony) = (None, None)
        (self.fp_per_combination, self.fn_per_combination, self.positive_per_combination,
         self.negative_per_combination) = (None, None, None, None)
        self.note_names = list(itertools.chain(*list(list(notename + str(number) for notename in
                                                          ['C', 'Cis', 'D', 'Dis', 'E', 'F', 'Fis', 'G', 'Gis', 'A',
                                                           'Ais', 'B']) for number in range(1, 9))))
        # print self.note_names

    def addTest(self, threshold, test_accuracy, test_precision, test_recall, test_f1):
        self.tests[threshold] = [self.model_name, threshold, test_accuracy, test_precision, test_recall, test_f1]

    def add_fault_position_metrics(self, fn_before, fn_after, fp_before, fp_after, present_tp, present_fp, present_fn,
                                   tp_len, fp_len, fn_len):
        self.fn_before += fn_before
        self.fn_after += fn_after
        self.fp_before += fp_before
        self.fp_after += fp_after
        self.present_fp += present_fp
        self.present_tp += present_tp
        self.present_fn += present_fn
        self.fault_metrics += np.array([tp_len, fp_len, fn_len])

    def add_faults_per_note(self, tp_per_note, fp_per_note, fn_per_note, tn_per_note):
        if self.tp_per_note is None:
            (self.tp_per_note, self.fp_per_note, self.fn_per_note, self.tn_per_note) = (
                tp_per_note, fp_per_note, fn_per_note, tn_per_note)
        else:
            (self.tp_per_note, self.fp_per_note, self.fn_per_note, self.tn_per_note) = [sum(x) for x in zip((
                tp_per_note, fp_per_note, fn_per_note, tn_per_note),
                (self.tp_per_note, self.fp_per_note, self.fn_per_note, self.tn_per_note))]

    def add_fault_combination_matrix(self, fp_per_combination, fn_per_combination, pos_per_combination,
                                     neg_per_combination):
        if self.fp_per_combination is None:
            self.fp_per_combination = fp_per_combination
            self.fn_per_combination = fn_per_combination
            self.positive_per_combination = pos_per_combination
            self.negative_per_combination = neg_per_combination
        else:
            self.fp_per_combination += fp_per_combination
            self.fn_per_combination += fn_per_combination
            self.positive_per_combination += pos_per_combination
            self.negative_per_combination += neg_per_combination

    def add_faults_per_polyphony(self, fp_per_polyphony, fn_per_polyphony,examples):
        if self.fn_per_polyphony is None:
            self.fp_per_polyphony = fp_per_polyphony
            self.fn_per_polyphony = fn_per_polyphony
            self.examples_polyphony = examples
        else:
            self.fp_per_polyphony += fp_per_polyphony
            self.fn_per_polyphony += fn_per_polyphony
            self.examples_polyphony += examples

    def write_to_csv(self):
        with open(system_settings.get_net_stats_path(self.model_name) + "fault_metrics.csv",
                  'wb') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter=',',
                                    quotechar='|', quoting=csv.QUOTE_MINIMAL)
            spamwriter.writerow(
                ["notenumber"] + range(1,89) + ['total'])
            spamwriter.writerow(
                ["false positives before"] + self.fp_before.tolist() + [self.fp_before.sum()])
            spamwriter.writerow(
                ["false positives after"] + self.fp_after.tolist() + [self.fp_after.sum()])
            spamwriter.writerow(
                ["false negatives before"] + self.fn_before.tolist()+ [self.fn_before.sum()])
            spamwriter.writerow(
                ["false negatives after"] + self.fn_after.tolist()+ [self.fn_after.sum()])
            spamwriter.writerow(
                ["fp"] + self.present_fp.tolist()+ [self.present_fp.sum()])
            spamwriter.writerow(
                ["fn"] + self.present_fn.tolist()+ [self.present_fn.sum()])
            spamwriter.writerow(
                ["tp"] + self.present_tp.tolist()+ [self.present_tp.sum()])
        with open(system_settings.get_net_stats_path(self.model_name) + str(self.test_file) + ".csv",
                  'wb') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter=',',
                                    quotechar='|', quoting=csv.QUOTE_MINIMAL)
            spamwriter.writerow(
                ["model name", "tresshold", "test accuracy", "test precision", "test recall", "test F1"])
            for test_model in self.tests:
                spamwriter.writerow(self.tests[test_model])
        with open(system_settings.get_net_stats_path(self.model_name) + "faults_per_note.csv",
                  'wb') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter=',',
                                    quotechar='|', quoting=csv.QUOTE_MINIMAL)
            for name, per_note in zip(('TP', 'FP', 'FN', 'TN'),
                                      (self.tp_per_note, self.fp_per_note, self.fn_per_note, self.tn_per_note)):
                spamwriter.writerow([name] + per_note.tolist())
        with open(system_settings.get_net_stats_path(self.model_name) + "fault_analysis_polyphony.csv",
                  'wb') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter=',',
                                    quotechar='|', quoting=csv.QUOTE_MINIMAL)
            spamwriter.writerow(["false positives "] + self.fp_per_polyphony.tolist())
            spamwriter.writerow(["false negatives "] + self.fn_per_polyphony.tolist())
            spamwriter.writerow(
                ["examples "] + self.examples_polyphony.tolist())
        for fault_name, fault_per_combination, fault_per_data in zip(("fp", "fn"), (
                self.fp_per_combination, self.fn_per_combination), (self.negative_per_combination,
                                                                    self.positive_per_combination)):
            with open(system_settings.get_net_stats_path(self.model_name) + str(fault_name) + "_per_combination.dat",
                      'w') as fault_file:
                for i in range(fault_per_combination.shape[0]):
                    for j in range(fault_per_combination.shape[1]):
                        fault_file.write(str(i) + ' ' + str(j) + ' ' + str(float(fault_per_combination[i, j])) + '\n')
                    fault_file.write('\n')
            faults_per_distance = np.zeros(shape=(63))
            comb_per_data = np.zeros(shape=(63))
            for i in range(fault_per_combination.shape[0]):
                for j in range(63):
                    index = i + j - 31
                    if index >= 0 and index < fault_per_combination.shape[1]:
                        # the fault is at index i, we check the positives at index i+j-31
                        faults_per_distance[j] += fault_per_combination[i, index]
                        # positives/negatives at distance j-31 from the note with index i
                        comb_per_data[j] += fault_per_data[i, index]
            with open(system_settings.get_net_stats_path(self.model_name) + str(
                    fault_name) + "_per_combination_distribution.dat",
                      'w') as fault_file:
                fault_file.write("fault_at percentage \n")
                #no division by zero
                comb_per_data += 1
                for i in range(63):
                    fault_file.write(str(-31+i) +' '+ str(faults_per_distance[i]) + '\n')
                # what we write is the percentage of positives at distance i from a negative where the negative is predicted as a false positive
                # and the percentage of positives at distance i from a positive where the positive is predicted as a false negative
                # so what we want to approach is P(fault at distance i from this note |  this note is predicted positive) where we divide the kind of faults in false positives and false negatives
            if fault_name == "fp":
                comb = "negatives"
            else:
                comb = "positives"
            with open(system_settings.get_net_stats_path(self.model_name) + comb +"_combination_distribution.dat",
                      'w') as comb_file:
                comb_file.write("fault_at percentage \n")
                # no division by zero
                for i in range(61):
                    comb_file.write(str(-30 + i) + ' ' + str(comb_per_data[i]) + '\n')

