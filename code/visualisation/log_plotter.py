'''
Title           :plot_learning_curve.py
Description     :This script generates learning curves for caffe models
Author          :Adil Moujahid
Date Created    :20160619
Date Modified   :20160619
version         :0.1
usage           :python plot_learning_curve.py model_1_train.log ./caffe_model_1_learning_curve.png
python_version  :2.7.11
source          : https://github.com/adilmoujahid/deeplearning-cats-dogs-tutorial/blob/master/code/plot_learning_curve.py
'''

import csv

import matplotlib
import pandas as pd

from visualisation import plot_1d_frame

matplotlib.use('Agg')
import matplotlib.pylab as plt
import system_settings
import numpy as np

plt.style.use('ggplot')


def plot_training_curve(model_name):
    # Parsing training/validation logs
    train_log = pd.read_csv(system_settings.get_net_stats_path(model_name) + "training.csv", 'wb', delimiter='\s*,\s*')
    make_train_curve(model_name, train_log, "train loss", "test loss", "loss")
    make_train_curve(model_name, train_log, "test accuracy", "test F1", "accuracy", ymax=1)
    make_train_curve(model_name, train_log, "test recall", "test precision", "precision&recall", ymax=1)
    '''
    Making learning curve
    '''


def make_train_curve(model_name, train_log, first_param, second_param, param_name, ymax=5):
    print train_log
    fig, ax1 = plt.subplots()
    # Plotting training and test losses
    first, = ax1.plot(train_log.index.values, train_log[first_param], color='red', alpha=.5)
    second, = ax1.plot(train_log.index.values, train_log[second_param], linewidth=2, color='green')
    ax1.set_ylim(ymin=0, ymax=ymax)
    ax1.set_xlabel('Iteration', fontsize=15)
    ax1.set_ylabel('Loss', fontsize=15)
    ax1.tick_params(labelsize=15)
    plt.legend([first, second],
               [first_param, second_param],
               bbox_to_anchor=(1, 0.7))
    plt.title('Training Curve ' + param_name, fontsize=18)
    # Saving learning curve
    plt.savefig(system_settings.get_visualisation_path(model_name) + "learning_curve_" + param_name + ".png")


def plot_fault_analysis(model_name):
    dict = {"FN": "false_negatives", "FP": "false_positives", "TP": "true_positives", "TN": "true_negatives"}
    with open(system_settings.get_net_stats_path(model_name) + "fault_analysis.csv", 'rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        for row in spamreader:
            title = dict[row[0]]
            faults = [int(i) for i in row[1:]]
            plot_1d_frame(np.array(faults), title, savefolder=system_settings.get_visualisation_path(model_name),
                          ylabel="Number of " + title, xlabel="MIDI note number")


def plot_precision_recall(models,fig_name,title = 'Precision recall diagram'):
    """
    plots the precision recall of different models and saves it with the specified threshold
    :param a python dictionary : the keys are the names that will be specified in the legend, the values are the test files to plot (with different thresholds)
    :return:
    """
    fig, ax1 = plt.subplots()
    plots = {}
    colors = ['b','g','r','c','m','y','k','w']
    for index,model_name in enumerate(models.keys()):
        test_log = pd.read_csv(system_settings.get_AMT_root() + "models/" + models[model_name] + '.csv', 'wb', delimiter='\s*,\s*')
        test_log[["test precision", "test recall"]] = test_log[["test precision", "test recall"]].apply(pd.to_numeric)
        test_log = test_log.sort_values("test precision")
        plots[model_name], = ax1.plot(test_log["test precision"], test_log["test recall"], alpha=.5,color=colors[index])

    ax1.set_ylim(ymin=0, ymax=1)
    ax1.set_xlim(xmin=0, xmax=1)
    ax1.set_xlabel('precission', fontsize=15)
    ax1.set_ylabel('recall', fontsize=15)
    ax1.tick_params(labelsize=15)
    plt.title(title, fontsize=18)
    # Saving learning curve
    keys = plots.keys()
    plt.legend([plots[key] for key in keys],
               keys,
               bbox_to_anchor=(1.0, 0.2))
    plt.savefig(system_settings.get_AMT_root() + "visualisations/" + fig_name + '.png')


def plot_metrics(model_name):
    fig, ax1 = plt.subplots()
    plots = {}
    test_log = pd.read_csv(system_settings.get_net_stats_path(model_name) + "test.csv", 'wb', delimiter='\s*,\s*')
    test_log[["tresshold", "%fn before", "%fn after", "%fp before", "%fp after"]] = test_log[
        ["tresshold", "%fn before", "%fn after", "%fp before", "%fp after"]].apply(pd.to_numeric)
    test_log = test_log.sort_values("tresshold")
    print test_log
    fn_after, = ax1.plot(test_log["tresshold"], test_log["%fn after"], alpha=.5)
    fn_before, = ax1.plot(test_log["tresshold"], test_log["%fn before"], alpha=.5)
    fp_after, = ax1.plot(test_log["tresshold"], test_log["%fp after"], alpha=.5)
    fp_before, = ax1.plot(test_log["tresshold"], test_log["%fp before"], alpha=.5)

    ax1.set_ylim(ymin=0, ymax=1)
    ax1.set_xlim(xmin=0, xmax=1)
    ax1.set_xlabel('threshold', fontsize=15)
    ax1.set_ylabel('percentage', fontsize=15)
    ax1.tick_params(labelsize=15)
    plt.title('Metrics', fontsize=18)
    # Saving learning curve
    keys = plots.keys()
    plt.legend([fn_after, fn_before, fp_after, fp_before],
               ["%fn after", "%fn before", "%fp after", "%fp before"],
               bbox_to_anchor=(1, 0.2))
    plt.savefig(system_settings.get_visualisation_path(model_name) + "metrics.png")
