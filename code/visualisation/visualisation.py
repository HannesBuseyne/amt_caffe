'''
Created on Mar 13, 2016

@author: Daan Seynaeve
'''

import warnings

import matplotlib as mpl
import numpy as np

mpl.use('Agg')
from matplotlib import pyplot as plt
import colormaps as cmaps

plt.register_cmap(name='plasma', cmap=cmaps.plasma)
plt.register_cmap(name='inferno', cmap=cmaps.inferno)
plt.register_cmap(name='viridis', cmap=cmaps.viridis)


# import seaborn

def note2octave(note_freq):
    oct_freq = np.zeros(9)
    for i in range(0, np.size(note_freq)):
        j = (i + 9) / 12
        print(j)
        oct_freq[j] += note_freq[i]
    return (oct_freq)


def plot_matrix(data, f_vals=None, shared=False, cmap='gray', title="", savefolder=None, format=".png", t_vals=None):
    '''
    visualize data such as: weight matrices, spectrograms, ...
    '''

    warnings.filterwarnings("ignore")
    if shared:
        data = data.get_value(borrow=True)

    if f_vals is None:
        f_vals = np.asarray([i for i in range(np.shape(data)[1])])
    if t_vals is None:
        t_vals = np.asarray([i for i in range(np.shape(data)[0])])
    fig = plt.figure()
    fig.suptitle(title)

    plt.pcolormesh(t_vals, f_vals, data.T, cmap=cmap)
    plt.axis('tight')

    plt.colorbar()

    if not savefolder == None:
        fig.savefig(savefolder + title + format)
        plt.close(fig)

    warnings.filterwarnings("default")


def plot_spectrogram(X, f_scale=None, t_scale=None, cmap='viridis',
                     title="Spectrogram", savefolder=None, format=".png",
                     show_colorbar=True):
    '''
    plot a spectrogram fit for reporting
    '''

    if f_scale is None:
        f_scale = np.asarray([i for i in range(np.shape(X)[1])])
    if t_scale is None:
        t_scale = np.asarray([i for i in range(np.shape(X)[0])])

    # -----

    fig = plt.figure(facecolor="#EFEFEF")
    fig.suptitle(title)

    plt.pcolormesh(t_scale, f_scale, X.T, cmap=cmap)
    plt.axis('tight')

    plt.ylabel('Frequency [Hz]')
    plt.xlabel('Time [s]')

    if show_colorbar:
        plt.colorbar().set_label('Energy')

    # -----

    if not savefolder == None:
        fig.savefig(savefolder + title + format)
        plt.close(fig)


def plot_weightmatrix(W, title="Weights", show_colorbar=True,
                      savefolder=None, format=".png",xlabel='x',ylabel='h1'):
    '''
    plot a weight matrix
    '''

    fig = plt.figure(facecolor="#EFEFEF")
    fig.suptitle(title)
    # plt.box("off")
    plt.tick_params(top="off", bottom="off", left="off", right="off")

    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    # ax = fig.add_axes([0, 0, 1, 1])

    pcm = plt.pcolormesh(W, cmap="gray", vmin=np.min(W))
    plt.axis('tight')
    if show_colorbar:
        fig.colorbar(pcm, extend='both')
    if not savefolder == None:
        fig.savefig(savefolder + title + format)
        plt.close(fig)
    else:
        plt.show()

def weightmatrix_to_pgfplots(W,savefolder,title,format=".dat"):
    with open(savefolder + title + format,'w') as plotfile:
        for i in range(W.shape[0]):
            for j in range(W.shape[1]):
                plotfile.write(str(i) + ' ' + str(j) +' '+str(W[i,j]) + '\n')
            plotfile.write('\n')



def plot_weightmatrix_color(W, title="Weights", show_colorbar=False,
                            savefolder=None, format=".png"):
    '''
    plot a weight matrix
    '''

    fig = plt.figure(facecolor="#EFEFEF")
    fig.suptitle(title)
    # plt.box("off")
    plt.tick_params(top="off", bottom="off", left="off", right="off")

    plt.xlabel("x")
    plt.ylabel("h1")
    # ax = fig.add_axes([0, 0, 1, 1])
    pcm = plt.pcolormesh(W, cmap="rainbow", vmin=-np.max(W))
    fig.colorbar(pcm, extend='both')
    plt.axis('tight')
    if not savefolder == None:
        fig.savefig(savefolder + title + format)
        plt.close(fig)
    else:
        plt.show()

        # fig.ax.spines['top'].set_visible(False)



def plot_1d_frame(frame, title="Filter", savefolder=None, format=".png", ylabel="filter index",
                  xlabel="filter magnitude"):
    N = frame.shape[0]
    ind = np.arange(N)  # the x locations for the groups
    width = 1  # the width of the bars

    fig, ax = plt.subplots()
    rects1 = ax.bar(ind, frame, width, color='r')

    # add some text for labels, title and axes ticks
    ax.set_ylabel(ylabel)
    plt.xlabel(xlabel)
    ax.set_title(title)
    if not savefolder == None:
        fig.savefig(savefolder + title + format)
        plt.close(fig)
    else:
        plt.show()

def oned_frame_to_pgfplots(frame,savefolder,title,format=".dat",xlabel='x',ylabel='y'):
    with open(savefolder + title + format,'w') as plotfile:
        plotfile.write(str(xlabel) + ' ' + str(ylabel) + '\n')
        for i in range(frame.shape[0]):
            plotfile.write(str(i) + ' ' + str(frame[i]) + '\n')

if __name__ == '__main__':
    pass
