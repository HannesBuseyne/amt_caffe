AMT_ROOT="/export/home1/NoCsBack/dtai/r0363226/AMT_caffe"
NET="5_layer"
label="notes"
for part in "SP_1" "SP_2" "SP_3" "SP_4"
do
	python $AMT_ROOT/code/runner/runner.py "MusicNet" $part "$AMT_ROOT/config/$label/$NET/defaultconfig.conf" &
done
wait

for part in "SP_1" "SP_2" "SP_3" "SP_4"
do
	python $AMT_ROOT/code/runner/runner.py "MusicNet" $part "$AMT_ROOT/config/$label/$NET/precision_recall.conf" &
	python $AMT_ROOT/code/runner/runner.py "MusicNet" $part "$AMT_ROOT/config/$label/$NET/lin_moving_threshold.conf" &
	python $AMT_ROOT/code/runner/runner.py "MusicNet" $part "$AMT_ROOT/config/$label/$NET/exp_moving_threshold.conf" &
	python $AMT_ROOT/code/runner/runner.py "MusicNet" $part "$AMT_ROOT/config/$label/$NET/step_moving_threshold.conf" &
done
