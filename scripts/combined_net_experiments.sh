AMT_ROOT="/export/home1/NoCsBack/dtai/r0363226/AMT_caffe"
NET="5_layer"

#train network with smaller slices
sed -i "s/slice = 16384/slice = 4096/g" "$AMT_ROOT/config/$label/$NET/defaultconfig.conf"
for part in "MUS_1" "MUS_2" "MUS_3" "MUS_4"
do
	python $AMT_ROOT/code/runner/runner.py "MAPS" $part "$AMT_ROOT/config/combined/$NET/defaultcombined.conf"&
done

for part in "SP_1" "SP_2" "SP_3" "SP_4"
do
	python $AMT_ROOT/code/runner/runner.py "MusicNet" $part "$AMT_ROOT/config/combined/$NET/defaultcombined.conf"&
done
sed -i "s/slice = 4096/slice = 16384/g" "$AMT_ROOT/config/$label/$NET/defaultconfig.conf"

wait

#test it

for part in "MUS_1" "MUS_2" "MUS_3" "MUS_4"
do
	python $AMT_ROOT/code/runner/runner.py "MAPS" $part "$AMT_ROOT/config/combined/$NET/defaultcombined.conf"&
	python $AMT_ROOT/code/runner/runner.py "MAPS" $part "$AMT_ROOT/config/combined/$NET/combined_moving_threshold.conf"&
done

for part in "SP_1" "SP_2" "SP_3" "SP_4"
do
	python $AMT_ROOT/code/runner/runner.py "MusicNet" $part "$AMT_ROOT/config/combined/$NET/defaultcombined.conf"&
	python $AMT_ROOT/code/runner/runner.py "MusicNet" $part "$AMT_ROOT/config/combined/$NET/combined_moving_threshold.conf"&
done
