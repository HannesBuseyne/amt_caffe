AMT_ROOT="/workspace/AMT_caffe"
NET="2_layer"
label="notes"
for preproc in "daan_preprocessor" "normalizer" "framepreprocessor" "framenormaliser"
do
	sed -i "s/test = test/test = test_$preproc/g" "$AMT_ROOT/config/$label/$NET/defaultconfig.conf"
	sed -i "s/preprocessor = framenormalizer/preprocessor = $preproc/g" "$AMT_ROOT/config/$label/$NET/defaultconfig.conf"
	for part in "MUS" "Solo Piano"
	do
		python $AMT_ROOT/code/runner/runner.py "MusicNet" "$part" "$AMT_ROOT/config/$label/$NET/defaultconfig.conf" &
	done
	for part in "MUS_1" "MUS_2" "MUS_3" "MUS_4"
	do
		python $AMT_ROOT/code/runner/runner.py "MAPS" $part "$AMT_ROOT/config/$label/$NET/defaultconfig.conf" &
	done
	wait
	sed -i "s/test = test_$preproc/test = test/g" "$AMT_ROOT/config/$label/$NET/defaultconfig.conf"
	sed -i "s/preprocessor = $preproc/preprocessor = framenormalizer/g" "$AMT_ROOT/config/$label/$NET/defaultconfig.conf"
done
