AMT_ROOT="/workspace/AMT_caffe"
NET="2_layer"
for part in "MUS_1" "MUS_2" "MUS_3" "MUS_4"
do
	for label in "notes" "attention"
	do
		for slice_length in 2048 4096 8192 16384 32768
			do	
				sed -i "s/slice = 65536/slice = $slice_length/g" "$AMT_ROOT/config/$label/$NET/defaultconfig.conf"
				python $AMT_ROOT/code/runner/runner.py "MAPS" $part "$AMT_ROOT/config/$label/$NET/defaultconfig.conf"
				sed -i "s/slice = $slice_length/slice = 65536/g" "$AMT_ROOT/config/$label/$NET/defaultconfig.conf"
				sed -i "s/slice = 65536/slice = $slice_length/g" "$AMT_ROOT/config/$label/$NET/precision_recall.conf"
				python $AMT_ROOT/code/runner/runner.py "MAPS" "$part" "$AMT_ROOT/config/$label/$NET/precision_recall.conf"
				sed -i "s/slice = $slice_length/slice = 65536/g" "$AMT_ROOT/config/$label/$NET/precision_recall.conf"
			done
			python $AMT_ROOT/code/runner/runner.py "MAPS" $part "$AMT_ROOT/config/$label/$NET/defaultconfig.conf"
			python $AMT_ROOT/code/runner/runner.py "MAPS" "$part" "$AMT_ROOT/config/$label/$NET/precision_recall.conf"
			sed -i "s/DS_MAPS-MUS_1/DS_MAPS-$part/g" "$AMT_ROOT/config/visualization/$NET/slice_length_visualisation.conf"
			sed -i "s/labels_notes/labels_$label/g" "$AMT_ROOT/config/visualization/$NET/slice_length_visualisation.conf"
			python $AMT_ROOT/code/runner/visualizer.py "$AMT_ROOT/config/visualization/$NET/slice_length_visualisation.conf"
			sed -i "s/DS_MAPS-$part/DS_MAPS-MUS_1/g" "$AMT_ROOT/config/visualization/$NET/slice_length_visualisation.conf"
			sed -i "s/labels_$label/labels_notes/g" "$AMT_ROOT/config/visualization/$NET/slice_length_visualisation.conf"
			
	done
done

for part in "MUS" "Solo Piano"
do
	for label in "notes" "attention" 
	do
		for slice_length in 2048 4096 8192 16384 32768
			do	
				sed -i "s/slice = 65536/slice = $slice_length/g" "$AMT_ROOT/config/$label/$NET/defaultconfig.conf"
				python $AMT_ROOT/code/runner/runner.py "MusicNet" "$part" "$AMT_ROOT/config/$label/$NET/defaultconfig.conf"
				sed -i "s/slice = $slice_length/slice = 65536/g" "$AMT_ROOT/config/$label/$NET/defaultconfig.conf"
				sed -i "s/slice = 65536/slice = $slice_length/g" "$AMT_ROOT/config/$label/$NET/precision_recall.conf"
				python $AMT_ROOT/code/runner/runner.py "MusicNet" "$part" "$AMT_ROOT/config/$label/$NET/precision_recall.conf"
				sed -i "s/slice = $slice_length/slice = 65536/g" "$AMT_ROOT/config/$label/$NET/precision_recall.conf"
			done
			python $AMT_ROOT/code/runner/runner.py "MusicNet" "$part" "$AMT_ROOT/config/$label/$NET/defaultconfig.conf"
			python $AMT_ROOT/code/runner/runner.py "MusicNet" "$part" "$AMT_ROOT/config/$label/$NET/precision_recall.conf"
			sed -i "s/DS_MAPS-MUS_1/DS_MusicNet-$part/g" "$AMT_ROOT/config/visualization/$NET/slice_length_visualisation.conf"
			sed -i "s/labels_notes/labels_$label/g" "$AMT_ROOT/config/visualization/$NET/slice_length_visualisation.conf"
			python $AMT_ROOT/code/runner/visualizer.py "$AMT_ROOT/config/visualization/$NET/slice_length_visualisation.conf"
			sed -i "s/DS_MusicNet-$part/DS_MAPS-MUS_1/g" "$AMT_ROOT/config/visualization/$NET/slice_length_visualisation.conf"
			sed -i "s/labels_$label/labels_notes/g" "$AMT_ROOT/config/visualization/$NET/slice_length_visualisation.conf"
	done
done
